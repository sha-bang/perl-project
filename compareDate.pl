#Source : http://perlmeme.org/faqs/datetime/comparing_dates.html

#!/usr/bin/perl
#Uses Time::Local Module
#compareDate.pl


#Fall Semester Date Comparison
use strict;
use warnings;
use Time::Local;

my (@today,
	$time,
	@deadFeeFall,
	$deadFeeFall,
	$untilDeadDayFall,
	$untilDeadHourFall);

@today = localtime();
$time = timelocal(@today);

@deadFeeFall = (0, 0, 0, 7, 8, 2015);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
$deadFeeFall = timelocal(@deadFeeFall);
	
$untilDeadDayFall = (($deadFeeFall - $time) / 86400);
	

if ($untilDeadDayFall < 1)
{
	$untilDeadHourFall = (($deadFeeFall - $time) / 3600);
	printf "There is %.2f hour(s) until the fee deadline\n", $untilDeadHourFall;
}
else 
{
	printf "There is %.0f day(s) until the fee deadline for fall semester students \n(September 7, 2015 @ 12:00 am).\n", 		$untilDeadDayFall;
}

    
#***********************************************************************************************
#Winter Semester fee deadline comparison

my (@deadFeeWin,
	$deadFeeWin,
	$untilDeadDayWin,
	$untilDeadHourWin);

@today = localtime();
$time = timelocal(@today);

@deadFee = (0, 0, 0, 7, 8, 2015);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
$deadFee = timelocal(@deadFee);
	
$untilDeadDay = (($deadFee - $time) / 86400);
	

if ($untilDeadDay < 1)
{
	$untilDeadHour = (($deadFee - $time) / 3600);
	print "\$time - \$deadFee = " . ($time - $deadFee) . " sec\n";
	print "\$deadFee = $deadFee sec\n";
	print "\$time = $time sec\n";
	printf "There is %.2f hour(s) until the fee deadline\n", $untilDeadHour;
}
else 
{
	printf "There is %.0f day(s) until the fee deadline for fall semester students \n(September 7, 2015 @ 12:00 am).\n", 		$untilDeadDay;
}

