#!/usr/bin/perl
#testSplice.pl
#References : 
#	[1]Hagi Elghahagi – March 17, 2011
#	[2]https://askubuntu.com/questions/434051/how-to-replace-a-string-on-the-5th-line-of-multiple-text-files 
use strict;
use warnings;

my ( $newCourse, $courseDrop, $idtocheck, $aryCourseIndex, $aryIndex, @aryCourse, $lengthAryCourse, $lengthAry, $noCourse, @aryLine, $aryLine, $aryCourse, @aryNew, $aryNew, $aryEndIndex);

$aryNew = "";
$aryCourse = "";
$aryEndIndex = 0;

###################################################################################################################
#	ADDING A COURSE TO COURSEREG 
###################################################################################################################
sub addCourse
{
$noCourse = "X";
$aryIndex = -1;

open (FH, "courseReg.txt") || die "Can't read file: $!\n";
chomp(@aryLine = <FH>);
close (FH);

print "Enter student id: \n";
chomp ($idtocheck = <STDIN>);

print "Enter course to add: \n";
chomp ($newCourse = <STDIN>);


foreach $aryLine(@aryLine)	# array using each line in the file as an element

{
	@aryCourse = split( /\s/, $aryLine);	#split each line by a whitespace
	
	
	$aryIndex++;
	print "Index $aryIndex and Line ", ($aryIndex + 1), " iteration.\n";
	
	$lengthAryCourse = @aryCourse;

	foreach (@aryCourse)				#for each element in each line which is separated by a space
	{   

   		if (/$idtocheck/i)						#if the student ID entered matches the element on the line
   		{


           	for ($aryCourseIndex = 0; $aryCourseIndex < $lengthAryCourse; $aryCourseIndex++)	# iterate through each element
           	{
           		print "Index = $aryCourseIndex, Element = $aryCourse[$aryCourseIndex]\n";
           		
      			if ($aryCourse[$aryCourseIndex] eq $noCourse)	# if an unregistered course is found
          		{
          			
          			print "Spliced on line ", ($aryIndex + 1),"\n";
          			$aryEndIndex = $aryIndex;
          			splice (@aryCourse, $aryCourseIndex, 1, $newCourse);	#splice and replace the unregistered course with newCourse
          			print "@aryCourse\n";
          			
          			$aryNew = join(' ', @aryCourse);
          			
          			splice (@aryLine, $aryEndIndex, 1, $aryNew);
          			
          				foreach $aryLine(@aryLine)	#foreach to reprint new array values into file
						{
	        				
						@aryNew = split( /\s/, $aryLine);
						print "@aryNew\n";
 						open (FH, "+<courseReg.txt") || die "Can't open file for read/write: $!\n"; 
						system(`sed -i "s/$aryCourse/$aryNew/" courseReg.txt`);
						close (FH);   			
						}
						
          			last;
          		}
          		else
          		{
          			if ($aryCourseIndex == 0)
          			{
          			print "$newCourse is already registered for the student.\n";
          			
          			}
          			$aryCourse = join(' ',@aryCourse);
          		}
          		

          	}
          				
    	}
	}

}


print "\$aryNew is\n$aryNew\n\n";
print "\$aryCourse is\n$aryCourse\n\n";
print "aryIndex after foreach = $aryEndIndex\n";


$lengthAry = @aryLine;

print "The new array size is $lengthAry\n";
print "The new array is\n\n",
		"@aryLine\n\n";
     			

 

print "Here are the new changes to the file \n\n";
system("cat", "courseReg.txt");

}	#end sub addCourse

###################################################################################################################
#	DROPPING A COURSE TO COURSEREG 
###################################################################################################################
sub dropCourse
{
$noCourse = "X";
$aryIndex = -1;

open (FH, "courseReg.txt") || die "Can't read file: $!\n";
chomp(@aryLine = <FH>);
close (FH);

print "Enter student id: \n";
chomp ($idtocheck = <STDIN>);

print "Enter course to drop: \n";
chomp ($courseDrop = <STDIN>);


foreach $aryLine(@aryLine)	# array using each line in the file as an element

{
	@aryCourse = split( /\s/, $aryLine);	#split each line by a whitespace
	
	
	$aryIndex++;
	print "Index $aryIndex and Line ", ($aryIndex + 1), " iteration.\n";
	
	$lengthAryCourse = @aryCourse;

	foreach (@aryCourse)				#for each element in each line which is separated by a space
	{   
   		if (/$idtocheck/i)						#if the student ID entered matches the element on the line
   		{  
           	
           	for ($aryCourseIndex = 0; $aryCourseIndex < $lengthAryCourse; $aryCourseIndex++)	# iterate through each element
           	{
           		print "Index = $aryCourseIndex, Element = $aryCourse[$aryCourseIndex]\n";
           		
      			if ($aryCourse[$aryCourseIndex] eq $courseDrop)	# if a registered course matches chosen course to drop
          		{
          			
          			print "Spliced on line ", ($aryIndex + 1),"\n";
          			$aryEndIndex = $aryIndex;
          			splice (@aryCourse, $aryCourseIndex, 1, $noCourse);	#splice and replace the registered course with noCourse, which is a capital X
          			print "@aryCourse\n";
          			
          			$aryNew = join(' ', @aryCourse);
          			last;
          		}
          		else
          		{
          			if ($aryCourseIndex == 1)
          			{
          			print "$courseDrop is not registered for the student.\n";
          			
          			}
          			else
          			{
          			$aryCourse = join(' ',@aryCourse);
          			}
          		}

          	}			
    	}
	}

}


print "\$aryNew is\n$aryNew\n\n";
print "\$aryCourse is\n$aryCourse\n\n";
print "aryIndex after foreach = $aryEndIndex\n";

splice (@aryLine, $aryEndIndex, 1, $aryNew);
$lengthAry = @aryLine;

print "The new array size is $lengthAry\n";
print "The new array is\n\n",
		"@aryLine\n\n";
     			
foreach $aryLine(@aryLine)	#foreach to reprint new array values into file
{
	        				
	@aryNew = split( /\s/, $aryLine);
	print "@aryNew\n";
	
    open (FH, "+<courseReg.txt") || die "Can't open file for read/write: $!\n"; 
	system(`sed -i "s/$aryCourse/$aryNew/" courseReg.txt`);
	close (FH);   			
}
 

print "Here are the new changes to the file \n\n";
system("cat", "courseReg.txt");
}	#end sub dropCourse


&addCourse;
&dropCourse;


