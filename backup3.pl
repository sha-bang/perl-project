#!/usr/bin/perl
#Name : Jesse Dela Rosa & Jervhee Dacio
#file : feeSource.pl
#Sources used : http://perlmeme.org/faqs/datetime/comparing_dates.html
#
use strict;
use warnings;


my ($studid, $fname, $lname, $school, $prog, $major, $sem, $studStat, $feeStat, $intStat, $check, @check, %checkhash, $checkhash, $idtocheck, $flag, $date, $now, @now, $opt);

my (@today,			#allow subroutines to share these variables
	$time, 
	$untilDayStart,
	$untilDayEnd,
	$untilHour);	

my (@feeNew,		#feeNew subroutine variables
	$feeNew,
	$untilDayNew,
	$untilHourNew);

my (@feeFall,		#feeFall subroutine variables
	$feeFall,
	$untilDayFall,
	$untilHourFall);
	
my (@feeWin,		#feeWin subroutine variables
	$feeWin,
	$untilDayWin,
	$untilHourWin);
	
my (@addropFallStart,	#addropFall subroutine variables
	@addropFallEnd,
	$addropFallStart,
	$addropFallEnd);
	
my (@addropWinterStart,	#adddropWin subroutine variables
	@addropWinterEnd,
	$addropWinterStart,
	$addropWinterEnd);

my (@withdrawFall,		#withdrawFall subroutine variables
	$withdrawFall,
	$untilWithdrawDayFall,
	$untilWithdrawHourFall);
	
my (@withdrawWin,		#withdrawWin subroutine variables
	$withdrawWin,
	$untilWithdrawDayWin,
	$untilWithdrawHourWin);


#############################################################################################
# SUBROUTINE for getting current DATE
#############################################################################################
sub currentDate
{
open (OUTFILE, ">currDate.rpt") || die "$!";
$now = `date`;	#backticks

@now = split / /,$now;					#extract variable date on spaces
 if ($now[2] eq "")						#if statement to check if day is one number
										#and one space or two numbers
 {
  @now = ($now[1],$now[3],$now[6]);		#2nd, 4th, and 7th nums from the system date
 }
 else
 {
  @now = ($now[1],$now[2],$now[5]);		#2nd, 3rd, and 6th nums from the system date
 }
chomp($date= join " ",@now);
print OUTFILE "$date\n";
}

#############################################################################################
# SUBROUTINE for checking fee deadline date FOR SEMESTER 1/NEW students
#############################################################################################
sub feeNew
{
use Time::Local;

@today = localtime();
$time = timelocal(@today);

@feeNew = (0, 0, 0, 5, 7, 2015);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
$feeNew = timelocal(@feeNew);
	
$untilDayNew = (($feeNew - $time) / 86400);
	

#if ($untilDayNew < 1)
#{
#	$untilHourNew = (($feeNew - $time) / 3600);
#	printf "There is %.2f hour(s) until the fee deadline\n", $untilHourNew;
#}
#else 
#{
#	printf "There is %.0f day(s) until the fee deadline for NEW fall semester students, which is August 5 of this year. \n.", 		$untilDayNew;
#}
}

#############################################################################################
# SUBROUTINE for checking fee deadline date FOR FALL SEMESTER students
#############################################################################################
sub feeFall
{
use Time::Local;


@today = localtime();
$time = timelocal(@today);

@feeFall = (0, 0, 0, 2, 8, 2015);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
$feeFall = timelocal(@feeFall);
	
$untilDayFall = (($feeFall - $time) / 86400);
	

 if ($untilDayFall < 1)
 {
	$untilHourFall = (($feeFall - $time) / 3600);
	printf "There is %.2f hour(s) until the fee deadline\n", $untilHourFall;
 }
 else 
 {
	printf "There is %.0f day(s) until the fee deadline for continuing fall semester students, which is September 2 of this year. \n.", 		$untilDayFall;
 }
}

#############################################################################################
# SUBROUTINE for checking fee deadline date FOR WINTER SEMESTER students
#############################################################################################
sub feeWin
{
use Time::Local;


@today = localtime();
$time = timelocal(@today);

@feeWin = (0, 0, 0, 5, 7, 2016);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
$feeWin = timelocal(@feeWin);
	
$untilDayWin = (($feeWin - $time) / 86400);
	

 if ($untilDayWin < 1)
 {
	$untilHourWin = (($feeWin - $time) / 3600);
	printf "There is %.2f hour(s) until the fee deadline\n", $untilHourWin;
 }
  elsif ($untilDayWin > 1)
 {
 	print "You are past the fee deadline, your seat has been removed. Please exit the program\n";
 }
 else 
 {
	printf "There is %.0f day(s) until the fee deadline for winter semester students, which is January 5 of next year. \n.", 		$untilDayWin;
 }
}

#############################################################################################
# SUBROUTINE for checking add/drop deadline date FOR SEM 1/NEW students or SEM 3/CONT students
#############################################################################################
sub addropFall
{
use Time::Local;

@today = localtime();
$time = timelocal(@today);

@addropFallStart = (0, 0, 0, 2, 8, 2015);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes from 0-11 because that is the number of months until January
@addropFallEnd = (0, 0, 0, 12, 8, 2015);

$addropFallStart = timelocal(@addropFallStart);
$addropFallEnd = timelocal(@addropFallEnd);
	
$untilDayStart = (($addropFallStart - $time) / 86400);
	
$untilDayEnd = (($addropFallEnd - $time) / 86400);

 if ($untilDayStart > 1)
 {
	printf "You have to wait until September 2 of this year to add or drop courses. There is currently %0.f day(s) until that start date for adding or dropping courses.\n", $untilDayStart;
 }
 elsif ($untilDayEnd >= 1 && $untilDayEnd <=10)
 {
	printf "You have until September 12 of this year to add or drop classes. There is currently %0.f day(s) until the deadline\n", $untilDayEnd;
 }
 elsif ($untilDayEnd < 1)
 {
	$untilDayEnd = (($addropFallEnd - $time) / 3600);
	printf "There is %.0f hour(s) until the add/drop deadline for fall semester students. \n", 		$untilDayEnd;
 }
 else
 {
	print "Sorry, the add/drop period has expired. You can no longer perform this action.\n";
 }
}

#############################################################################################
# SUBROUTINE for checking add/drop deadline date FOR SEM 2 and 4 CONT students
#############################################################################################
sub addropWin
{
use Time::Local;

@today = localtime();
$time = timelocal(@today);

@addropWinterStart = (0, 0, 0, 5, 11, 2016);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes from 0-11 because that is the number of months until January
@addropWinterEnd = (0, 0, 0, 16, 11, 2016);

$addropWinterStart = timelocal(@addropWinterStart);
$addropWinterEnd = timelocal(@addropWinterEnd);
	
$untilDayStart = (($addropWinterStart - $time) / 86400);
	
$untilDayEnd = (($addropWinterEnd - $time) / 86400);

 if ($untilDayStart > 1)
 {
	printf "You have to wait until December 5 of next year to add or drop courses. There is currently %0.f day(s) until that start date for adding or dropping courses.\n", $untilDayStart;
 }
 elsif ($untilDayEnd >= 1 && $untilDayEnd <=10)
 {
	printf "You have until December 16 of this year to add or drop classes. There is currently %0.f day(s) until the deadline\n", $untilDayEnd;
 }
 elsif ($untilDayEnd < 1)
 {
	$untilDayEnd = (($addropWinterEnd - $time) / 3600);
	printf "There is %.0f hour(s) until the add/drop deadline for winter semester students. \n", 		$untilDayEnd;
 }
 else
 {
	print "Sorry, the add/drop period has expired. You can no longer perform this action.\n";
 }

}

#############################################################################################
# SUBROUTINE for checking withdrawal deadline date FOR SEM 1 and 3 students
#############################################################################################
sub withdrawFall
{
use Time::Local;


@today = localtime();
$time = timelocal(@today);

@withdrawFall = (0, 0, 0, 12, 10, 2016);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
$withdrawFall = timelocal(@withdrawFall);
	
$untilWithdrawDayFall = (($withdrawFall - $time) / 86400);
	

 if ($untilWithdrawDayFall < 1)
 {
	$untilWithdrawHourFall = (($withdrawFall - $time) / 3600);
	printf "There is %.2f hour(s) until the fee deadline\n", $untilWithdrawHourFall;
 }
 else 
 {
	printf "There is %.0f day(s) until the fee deadline for continuing fall semester students, which is November 12 of this year. \n.", 		$untilWithdrawDayFall;
 }
}

#############################################################################################
# SUBROUTINE for checking withdrawal deadline date FOR SEM 1 and 3 students
#############################################################################################
sub withdrawWin
{
use Time::Local;


@today = localtime();
$time = timelocal(@today);

@withdrawWin = (0, 0, 0, 25, 2, 2016);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
$withdrawWin = timelocal(@withdrawWin);
	
$untilWithdrawDayWin = (($withdrawWin - $time) / 86400);
	

 if ($untilWithdrawDayWin < 1)
 {
	$untilWithdrawHourWin = (($withdrawWin - $time) / 3600);
	printf "There is %.2f hour(s) until the fee deadline\n", $untilWithdrawHourWin;
 }
 else 
 {
	printf "There is %.0f day(s) until the fee deadline for continuing fall semester students, which is March 25 of this year. \n.", 		$untilWithdrawDayWin;
 }
}

#############################################################################################
# SUBROUTINE for Prompting and getting Major and Course (SEMESTER 3)
#############################################################################################
sub prgMajandCourse3
{
my ($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5, %sem3hash, $sem3hash, $sem3, @sem3);

	if ("CS" eq $checkhash->{MAJ})
	{
		my ($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5, %sem3hash, $sem3hash, $sem3, @sem3);


		open (MYFILE, "sem3courseCS.txt");
		chop(@sem3 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem3(@sem3)
		{
			($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5) = split(/ /,$sem3);
			%sem3hash=();
			$sem3hash ={
							COURSE1 => $sem3course1,
							COURSE2 => $sem3course2,
							COURSE3 => $sem3course3,
							COURSE4 => $sem3course4,
							COURSE5 => $sem3course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem3hash->{COURSE1};
		printf "%s\n",$sem3hash->{COURSE2};
		printf "%s\n",$sem3hash->{COURSE3};
		printf "%s\n",$sem3hash->{COURSE4};
		printf "%s\n",$sem3hash->{COURSE5};
	}
	elsif ("SD" eq $sem3hash->{MAJ}) 
{
		my ($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5, %sem3hash, $sem3hash);


		open (MYFILE, "sem3courseSD.txt");
		chop(@sem3 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem3(@sem3)
		{
			($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5) = split(/ /,$sem3);
			%sem3hash=();
			$sem3hash ={
							COURSE1 => $sem3course1,
							COURSE2 => $sem3course2,
							COURSE3 => $sem3course3,
							COURSE4 => $sem3course4,
							COURSE5 => $sem3course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem3hash->{COURSE1};
		printf "%s\n",$sem3hash->{COURSE2};
		printf "%s\n",$sem3hash->{COURSE3};
		printf "%s\n",$sem3hash->{COURSE4};
		printf "%s\n",$sem3hash->{COURSE5};
	}
	elsif ("NS" eq $sem3hash->{MAJ}) 
	{
		my ($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5, %sem3hash, $sem3hash);


		open (MYFILE, "sem3courseNS.txt");
		chop(@sem3 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem3(@sem3)
		{
			($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5) = split(/ /,$sem3);
			%sem3hash=();
			$sem3hash ={
							COURSE1 => $sem3course1,
							COURSE2 => $sem3course2,
							COURSE3 => $sem3course3,
							COURSE4 => $sem3course4,
							COURSE5 => $sem3course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem3hash->{COURSE1};
		printf "%s\n",$sem3hash->{COURSE2};
		printf "%s\n",$sem3hash->{COURSE3};
		printf "%s\n",$sem3hash->{COURSE4};
		printf "%s\n",$sem3hash->{COURSE5};
	}
	else
	{
		my ($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5, %sem3hash, $sem3hash);


		open (MYFILE, "sem3courseTelecom.txt");
		chop(@sem3 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem3(@sem3)
		{
			($sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5) = split(/ /,$sem3);
			%sem3hash=();
			$sem3hash ={
							COURSE1 => $sem3course1,
							COURSE2 => $sem3course2,
							COURSE3 => $sem3course3,
							COURSE4 => $sem3course4,
							COURSE5 => $sem3course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem3hash->{COURSE1};
		printf "%s\n",$sem3hash->{COURSE2};
		printf "%s\n",$sem3hash->{COURSE3};
		printf "%s\n",$sem3hash->{COURSE4};
		printf "%s\n",$sem3hash->{COURSE5};
	}
	
} 


#############################################################################################
# SUBROUTINE for Prompting and getting Major and Course (SEMESTER 2)
#############################################################################################
sub prgMajandCourse2
{
my ($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5, %sem2hash, $sem2hash, @sem2, $sem2);

	if ("CS" eq $checkhash->{MAJ})
	{
		my ($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5, %sem2hash, $sem2hash, @sem2, $sem2);


		open (MYFILE, "sem2courseCS.txt");
		chop(@sem2 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem2(@sem2)
		{
			($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5) = split(/ /,$sem2);
			%sem2hash=();
			$sem2hash ={
							COURSE1 => $sem2course1,
							COURSE2 => $sem2course2,
							COURSE3 => $sem2course3,
							COURSE4 => $sem2course4,
							COURSE5 => $sem2course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem2hash->{COURSE1};
		printf "%s\n",$sem2hash->{COURSE2};
		printf "%s\n",$sem2hash->{COURSE3};
		printf "%s\n",$sem2hash->{COURSE4};
		printf "%s\n",$sem2hash->{COURSE5};
	}
	elsif ("SD" eq $sem2hash->{MAJ}) 
		{
		my ($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5, %sem2hash, $sem2hash);


		open (MYFILE, "sem2courseSD.txt");
		chop(@sem2 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem2(@sem2)
		{
			($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5) = split(/ /,$sem2);
			%sem2hash=();
			$sem2hash ={
							COURSE1 => $sem2course1,
							COURSE2 => $sem2course2,
							COURSE3 => $sem2course3,
							COURSE4 => $sem2course4,
							COURSE5 => $sem2course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem2hash->{COURSE1};
		printf "%s\n",$sem2hash->{COURSE2};
		printf "%s\n",$sem2hash->{COURSE3};
		printf "%s\n",$sem2hash->{COURSE4};
		printf "%s\n",$sem2hash->{COURSE5};
	}
	elsif ("NS" eq $checkhash->{MAJ}) 
			{
		my ($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5, %sem2hash, $sem2hash);


		open (MYFILE, "sem2courseNS.txt");
		chop(@sem2 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem2(@sem2)
		{
			($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5) = split(/ /,$sem2);
			%sem2hash=();
			$sem2hash ={
							COURSE1 => $sem2course1,
							COURSE2 => $sem2course2,
							COURSE3 => $sem2course3,
							COURSE4 => $sem2course4,
							COURSE5 => $sem2course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem2hash->{COURSE1};
		printf "%s\n",$sem2hash->{COURSE2};
		printf "%s\n",$sem2hash->{COURSE3};
		printf "%s\n",$sem2hash->{COURSE4};
		printf "%s\n",$sem2hash->{COURSE5};
	}
	else
	{
		my ($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5, %sem2hash, $sem2hash);


		open (MYFILE, "sem2courseTelecom.txt");
		chop(@sem2 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem2(@sem2)
		{
			($sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5) = split(/ /,$sem2);
			%sem2hash=();
			$sem2hash ={
							COURSE1 => $sem2course1,
							COURSE2 => $sem2course2,
							COURSE3 => $sem2course3,
							COURSE4 => $sem2course4,
							COURSE5 => $sem2course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem2hash->{COURSE1};
		printf "%s\n",$sem2hash->{COURSE2};
		printf "%s\n",$sem2hash->{COURSE3};
		printf "%s\n",$sem2hash->{COURSE4};
		printf "%s\n",$sem2hash->{COURSE5};
	}
	
}

#############################################################################################
# SUBROUTINE for Prompting and getting Major and Course (SEMESTER 4)
#############################################################################################
sub prgMajandCourse4
{
my ($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4);

	if ("CS" eq $checkhash->{MAJ})
	{
		my ($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4);


		open (MYFILE, "sem4courseCS.txt");
		chop(@sem4 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem4(@sem4)
		{
			($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4) = split(/ /,$sem4);
			%sem4hash=();
			$sem4hash ={
							COURSE1 => $sem4course1,
							COURSE2 => $sem4course2,
							COURSE3 => $sem4course3,
							COURSE4 => $sem4course4,
							COURSE5 => $sem4course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem4hash->{COURSE1};
		printf "%s\n",$sem4hash->{COURSE2};
		printf "%s\n",$sem4hash->{COURSE3};
		printf "%s\n",$sem4hash->{COURSE4};
		printf "%s\n",$sem4hash->{COURSE5};
	}
	elsif ("SD" eq $sem4hash->{MAJ}) 
		{
		my ($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4);


		open (MYFILE, "sem4courseSD.txt");
		chop(@sem4 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem4(@sem4)
		{
			($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4) = split(/ /,$sem4);
			%sem4hash=();
			$sem4hash ={
							COURSE1 => $sem4course1,
							COURSE2 => $sem4course2,
							COURSE3 => $sem4course3,
							COURSE4 => $sem4course4,
							COURSE5 => $sem4course5,
							};
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem4hash->{COURSE1};
		printf "%s\n",$sem4hash->{COURSE2};
		printf "%s\n",$sem4hash->{COURSE3};
		printf "%s\n",$sem4hash->{COURSE4};
		printf "%s\n",$sem4hash->{COURSE5};
	}
	elsif ("NS" eq $checkhash->{MAJ}) 
	{
		my ($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4);


		open (MYFILE, "sem4courseNS.txt");
		chop(@sem4 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem4(@sem4)
		{
			($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4) = split(/ /,$sem4);
			%sem4hash=();
			$sem4hash ={
							COURSE1 => $sem4course1,
							COURSE2 => $sem4course2,
							COURSE3 => $sem4course3,
							COURSE4 => $sem4course4,
							COURSE5 => $sem4course5,
							};
		
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem4hash->{COURSE1};
		printf "%s\n",$sem4hash->{COURSE2};
		printf "%s\n",$sem4hash->{COURSE3};
		printf "%s\n",$sem4hash->{COURSE4};
		printf "%s\n",$sem4hash->{COURSE5};
	}
	else
	{
		my ($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4);


		open (MYFILE, "sem4courseTelecom.txt");
		chop(@sem4 = <MYFILE>);
		close (MYFILE);
		$flag = 1;
		
		foreach $sem4(@sem4)
		{
			($sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5, %sem4hash, $sem4hash, @sem4, $sem4) = split(/ /,$sem4);
			%sem4hash=();
			$sem4hash ={
							COURSE1 => $sem4course1,
							COURSE2 => $sem4course2,
							COURSE3 => $sem4course3,
							COURSE4 => $sem4course4,
							COURSE5 => $sem4course5,
							};
		
		} #end foreach
		print "Here are your your Courses\n";
		printf "%s\n",$sem4hash->{COURSE1};
		printf "%s\n",$sem4hash->{COURSE2};
		printf "%s\n",$sem4hash->{COURSE3};
		printf "%s\n",$sem4hash->{COURSE4};
		printf "%s\n",$sem4hash->{COURSE5};
	}
	
}  


#############################################################################################
									# MAIN PROGRAM #
#############################################################################################

open (MYFILE, "mainFile.txt");
chop(@check = <MYFILE>);
close (MYFILE);
$flag = 1;

print "Enter the ID of the student wishing to check fees: ";
chomp($idtocheck=<STDIN>);

foreach $check(@check)
{
	($studid, $fname, $lname, $school, $prog, $major, $sem, $studStat, $feeStat, $intStat) = split(/ /,$check);
	%checkhash=();
	$checkhash = {
					ID =>		$studid,
					FNAME =>	$fname,
					LNAME =>	$lname,
					SCHOOL =>	$school,		
					PROG =>		$prog,
					MAJ =>		$major,
					SEM =>		$sem,
					SSTAT =>	$studStat,
					FSTAT =>	$feeStat,
					ISTAT =>	$intStat
					
				};
				
				if ($idtocheck eq $checkhash->{ID})
				{
									
				printf "Student ID : %s\nFirst Name : %s\nLast Name : %s\nSchool : %s\nProgram : %s\nMajor : %s\nSemester : %s\nStudent Status : %s\nFee Status : %s\nInternational Status : %s\n\n", 					$checkhash->{ID}, $checkhash->{FNAME}, $checkhash->{LNAME}, $checkhash->{SCHOOL}, $checkhash->{PROG}, $checkhash->{MAJ}, $checkhash->{SEM}, $checkhash->{SSTAT}, $checkhash->{FSTAT}, 					$checkhash->{ISTAT};
				 
				$flag = 0;

					if ("New" eq $checkhash->{SSTAT})
					{
						if ("Paid" eq $checkhash->{FSTAT})
						{
						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
						$opt = "null";
						chomp ($opt = <STDIN>);
				   
						while ($opt ne "Exit")	#Begin while loop for menu selection
						{
							if ($opt eq "Add")
							{
								&addropFall;	#check add/drop date for fall	
								if ($untilDayStart > 1)
								{
								printf "You have to wait until September 2 of this year to add or drop courses. There is currently %0.f day(s) until that start date for adding or dropping courses.\n", $untilDayStart;
								}
								elsif ($untilDayEnd >= 1 && $untilDayEnd <=10)
								{
								print "Student is eligible to add a course.";
								printf "Student has until September 12 of this year to add or drop classes. There is currently %0.f day(s) until the deadline\n", $untilDayEnd;
									if ("International" eq $checkhash->{ISTAT})
									{
									print "Each course will cost \$1396.30 because student is stated as international.";
									print "What course does the student wish to add: ";
									}
								}
								elsif ($untilDayEnd < 1)
								{
								$untilDayEnd = (($addropFallEnd - $time) / 3600);
								printf "There is %.0f hour(s) until the add/drop deadline for fall semester students. \n",$untilDayEnd;
								}
								else
								{
								print "Sorry, the add/drop period has expired. You can no longer perform this action.\n";
								}
							}
							elsif ($opt eq "Drop")
							{
								&addropFall;	#check add/drop date for fall	
								if ($untilDayStart > 1)
								{
								printf "You have to wait until September 2 of this year to add or drop courses. There is currently %0.f day(s) until that start date for adding or dropping courses.\n", 									$untilDayStart;
								}
								elsif ($untilDayEnd >= 1 && $untilDayEnd <=10)
								{
								printf "You have until September 12 of this year to add or drop classes. There is currently %0.f day(s) until the deadline\n", $untilDayEnd;
								}
								elsif ($untilDayEnd < 1)
								{
								$untilDayEnd = (($addropFallEnd - $time) / 3600);
								printf "There is %.0f hour(s) until the add/drop deadline for fall semester students. \n",$untilDayEnd;
								}
								else
								{
								print "Sorry, the add/drop period has expired. You can no longer perform this action.\n";
								}
						
							}
					 	 
						}	#end while

						}	#end if for New Student+Paid
						else	#else if the new student has not paid yet
						{
							&feeNew;
							if ($untilDayNew < 1)
							{
							$untilHourNew = (($feeNew - $time) / 3600);
							printf "There is %.2f hour(s) until the fee deadline.\n", $untilHourNew;
							}
							elsif ($untilDayNew > 1) 
							{
							printf "There is %.0f day(s) until the fee deadline for NEW fall semester students, which is August 5 of this year. Please make sure student pays before this date or their seat will be removed\n", 		$untilDayNew;
							}
							else
							{
							print "It is past the fee deadline, student seat has been removed.";
							}
						}		#end if for New Student+Unpaid
				
					} #end if for New student
					elsif ("Continuing" eq $checkhash->{SSTAT})
				{	
					if ("2" eq $checkhash->{SEM})
					{
						print "You are starting semester 2, if this is incorrect exit the program and inform student services\n";
		 				if ("Paid" eq $checkhash->{FSTAT})
		 				{
		 					print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 					chomp ($opt = <STDIN>);
		 					while ($opt ne "Exit") #Begin while loop for menu option
		 				 	{	 	
		 				  		if ($opt eq "Add")
		 				  		{
		 				  			&addropWin;
		 				  			if ($untilDayEnd >= 1 && $untilDayEnd <=10)
		 				  			{
		 				  				print "You are eligible to add a course/courses\n";
		 				  				if ("International" eq $checkhash->{ISTAT})
		 				  				{
		 				  					print "Each course will cost \$1396.3 because you are an international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse2;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  				else
		 				  				{
		 				  					print "Each course will cost \$587.79 because you are a non-international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse2;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  			}
		 				  			else
		 				  			{
		 				  				print "You are not eligible to add a course becasuse you are either past past the deadling or you have to wait\n";
		 				  				last; 	
		 				  			}
		 				  		
		 				  		} #end if add
		 				  		elsif ($opt eq "Drop")
		 				  		{
		 				  			&addropWin;
		 				  			if ($untilDayEnd >= 1 && $untilDayEnd <=10)
		 				  			{
		 				  				print "You are eligible to drop a course/courses\n";
		 				  				if ("International" eq $checkhash->{ISTAT})
		 				  				{
		 				  					print "Each course will cost \$1396.3 because you are an international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse2;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  				else
		 				  				{
		 				  					print "Each course will cost \$587.79 because you are a non-international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse2;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse2;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  			}
		 				  			else
		 				  			{
		 				  				print "You are not eligible to add a course becasuse you are either past past the deadling or you have to wait\n";
		 				  				last; 
		 				  			}
		 				  		}
		 				  		elsif ($opt eq "Withdraw")
		 				  		{
		 				  			&withdrawWin;
		 				  			print "Which class would you like to Withdraw: ";
		 				  		}
		 				  		else
		 				  		{  #error check
		 				  			print "Please enter a valid option, and make sure you match the case\n"; 
		 				  		}
		 	 			 	} #end while
		 	 			} #end if statement for paid
		 	 			
						 
		 				elsif ("Unpaid" eq $checkhash->{FSTAT})
		 				{
		 					print "You haven't paid your fees yet\n";
		 					&feeWin
		 				}
					} #end if semester 2
				
					elsif ("3" eq $checkhash->{SEM})
					{
						print "You are starting semester 3, if this is incorrect exit the program and inform student services\n";
						if ("Paid" eq $checkhash->{FSTAT})
		 				{
		 					print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 					chomp ($opt = <STDIN>);
		 					while ($opt ne "Exit") #Begin while loop for menu option
		 				 	{	 	
		 				  		if ($opt eq "Add")
		 				  		{
		 				  			&addropFall;
		 				  			if ($untilDayEnd >= 1 && $untilDayEnd <=10)
		 				  			{
		 				  				print "You are eligible to add a course/courses\n";
		 				  				if ("International" eq $checkhash->{ISTAT})
		 				  				{
		 				  					print "Each course will cost \$1396.3 because you are an international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse3;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  				else
		 				  				{
		 				  					print "Each course will cost \$587.79 because you are a non-international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse3;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  			}
		 				  			else
		 				  			{
		 				  				print "You are not eligible to add a course becasuse you are either past past the deadling or you have to wait\n";
		 				  				last; 	
		 				  			}
		 				  		
		 				  		} #end if add
		 				  		elsif ($opt eq "Drop")
		 				  		{
		 				  			&addropFall;
		 				  			if ($untilDayEnd >= 1 && $untilDayEnd <=10)
		 				  			{
		 				  				print "You are eligible to drop a course/courses\n";
		 				  				if ("International" eq $checkhash->{ISTAT})
		 				  				{
		 				  					print "Each course will cost \$1396.3 because you are an international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse3;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  				else
		 				  				{
		 				  					print "Each course will cost \$587.79 because you are a non-international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse3;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse3;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  			}
		 				  			else
		 				  			{
		 				  				print "You are not eligible to add a course becasuse you are either past past the deadling or you have to wait\n";
		 				  				last; 
		 				  			}
		 				  		}
		 				  		elsif ($opt eq "Withdraw")
		 				  		{
		 				  			&withdrawFall;
		 				  			print "Which class would you like to Withdraw: ";
		 				  		}
		 				  		else
		 				  		{  #error check
		 				  			print "Please enter a valid option, and make sure you match the case\n"; 
		 				  		}
		 	 			 	} #end while
		 	 			} #end if statement for paid
		 	 			
						 
		 				elsif ("Unpaid" eq $checkhash->{FSTAT})
		 				{
		 					print "You haven't paid your fees yet\n";
		 					&feeWin
		 				}
					} #end if semester 3
					elsif ("4" eq $checkhash->{SEM})
					{
		 				print "You are starting semester 4, if this is incorrect exit the program and inform student services\n";
						if ("Paid" eq $checkhash->{FSTAT})
		 				{
		 					print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 					chomp ($opt = <STDIN>);
		 					while ($opt ne "Exit") #Begin while loop for menu option
		 				 	{	 	
		 				  		if ($opt eq "Add")
		 				  		{
		 				  			&addropWin;
		 				  			if ($untilDayEnd >= 1 && $untilDayEnd <=10)
		 				  			{
		 				  				print "You are eligible to add a course/courses\n";
		 				  				if ("International" eq $checkhash->{ISTAT})
		 				  				{
		 				  					print "Each course will cost \$1396.3 because you are an international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse4;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  				else
		 				  				{
		 				  					print "Each course will cost \$587.79 because you are a non-international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse4;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  			}
		 				  			else
		 				  			{
		 				  				print "You are not eligible to add a course becasuse you are either past past the deadling or you have to wait\n";
		 				  				last; 	
		 				  			}
		 				  		
		 				  		} #end if add
		 				  		elsif ($opt eq "Drop")
		 				  		{
		 				  			&addropWin;
		 				  			if ($untilDayEnd >= 1 && $untilDayEnd <=10)
		 				  			{
		 				  				print "You are eligible to drop a course/courses\n";
		 				  				if ("International" eq $checkhash->{ISTAT})
		 				  				{
		 				  					print "Each course will cost \$1396.3 because you are an international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse4;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  				else
		 				  				{
		 				  					print "Each course will cost \$587.79 because you are a non-international student\n";
		 				  					if ("CS" eq $checkhash->{MAJ})
		 				  					{
		 				  						&prgMajandCourse4;
		 				  						print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("SD" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}
											elsif ("NS" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
		 				  					}
		 				  					elsif ("Telecom" eq $checkhash->{MAJ})
											{
												&prgMajandCourse4;
												print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
		 										chomp ($opt = <STDIN>);
											}	
		 				  				}
		 				  			}
		 				  			else
		 				  			{
		 				  				print "You are not eligible to add a course becasuse you are either past past the deadling or you have to wait\n";
		 				  				last; 
		 				  			}
		 				  		}
		 				  		elsif ($opt eq "Withdraw")
		 				  		{
		 				  			&withdrawWin;
		 				  			print "Which class would you like to Withdraw: ";
		 				  		}
		 				  		else
		 				  		{  #error check
		 				  			print "Please enter a valid option, and make sure you match the case\n"; 
		 				  		}
		 	 			 	} #end while
		 	 			} #end if statement for paid
		 	 			
						 
		 				elsif ("Unpaid" eq $checkhash->{FSTAT})
		 				{
		 					print "You haven't paid your fees yet\n";
		 					&feeWin
		 				} # end elsif unpaid
					} #end if semester 4
					else
					{
 		 			 	print "invalid";
					}
				} #end elsif continuing
			 	elsif ("Expelled" eq $checkhash->{SSTAT})
				{
					print "You are not eligible to use this program because the ID entered is expelled\n";
				}
		} #end if statement for id check
	} #end foreach
if ($flag==1)
{
	print "That Student ID is currently unregistered. Please make sure that student is enrolled or that you have entered the correct 9 digit student ID \n";
}
				
