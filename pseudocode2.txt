PSEUDOCODE
Prompt and get ID
Read INFILE 
Print information on INFILE
If studentstatus == “newstudent”

	If feestatus == “paid”
	  Print “What do you choose to do:  Add/Drop/Withdraw/exit”     (Copy1)

	While != “exit”
		If “add”
		  call subroutine for adddropFall date check

			If date is before 2 weeks of start of term
			  Print “Eligible to add a course/class”
				If internatStat == “international”
				  Print “Each course will cost $1396.3 because you are an international student”
				  Prompt and get course/courses to be added		#hash for newstudent courses AKA semester 1
				  Print “Course chosen and total cost”
				  Add course to hash and outfile courseReg
				  Add total cost to fees
				  Print “Your new total fees is $fees”
				  Change feestatus to unpaid

				Else
				  Print “Each course will cost $587.89 because you are a non-	international student”
				  Prompt and get course/courses to be added		#hash newstudent courses AKA semester 1
				  Print “Course chosen and total cost”
				  Add course to hash and outfile courseReg
				  Add total cost to fees
				  Print “Your new total fees is $fees”
				  Change feestatus to unpaid

			Else
			  Print “You are not eligible to add a course because you are past the deadline”

		Elseif “Drop”
		  call subroutine for adddropFall date check

			If date is before 2 weeks of start of term
			  Print “Eligible to drop a course”

				If internatStat == “international”
				  check hash and outfile courseReg for number of courses currently in hash

					If courseAmt >= 3
					  Print “Each course will cost $1396.3 because you are 	an international student”
					  Prompt and get course/courses to be dropped	#hash 	newstudent courses and print out courses for sem1
					  Print “Course chosen and total cost”
					  Match stdin to hash in courseReg and remove
					  Deduct totalcost to fees
					  Print “Your new total fees is $fees”

					Else
					  Print “You need at least 3 courses to continue either add or withdraw”

				Else
				  Print “Each course will cost $587.89 because you are a non-	international student”
				  Prompt and get course/courses to be dropped	#hash newstudent courses and print out courses for sem1
				  Print “Course chosen and total cost”
				  Match stdin to hash in courseReg and remove
				  Deduct totalcost to fees
				  Print “You are not eligible to drop a course because you are past the deadline”

		Elseif “Withdraw”
		  call subroutine for withdrawFall date check

			If today is before Nov 12
			  Print “You are eligible to withdraw”

				If today is before 2 weeks of start of term 
				  Print “You are also eligible for a full refund”
				  Prompt and get confirmation of their withdrawal
				  Print “You will receive an email confirming your withdrawal”

				Elsif today is between progStartFall and adddropFall
				  Print “You will receive a 50% because you are past the first day of classes”
				  Prompt and get confirmation of their withdrawal
				  Print “You will receive an email confirming your withdrawal”
				Else
				Print “you are past the add/drop period so you are not eligible for a refund”
				Prompt and get confirmation of their withdrawal
				Print “You will receive an email confirming your withdrawal”
			Else
			Print “You are not eligible to withdraw because you are past the deadline”

	Else
	  call subroutine to check feeDeadFallNew
		If today is before Aug 5
		  Print “You fees are still unpaid, You have until Aug 5 to pay”
		(Copy 1)
		Else
		  Print “You are past the fee deadline, your seat has been removed”

Elsif studentstatus == “continuing”
If sem == 2
	 Print “You are starting semester 2, if this is incorrect, exit the program and inform student services”
		If feestatus == “paid”
		 Print “What do you choose to do:  Add/Drop/Withdraw/exit”
		 While != “exit”
			If “add”
		 	 call subroutine for adddropWinter date check
			 If date is before 2 weeks of start of term
			 Print “Eligible to add a course/class”
				If internatStat == “international”
				  Print “Each course will cost $1396.3 because you are an international student”
				  Prompt and get Major
				  	If major == “CS”
				 	 Prompt and get course/courses to be added #hash for CS courses AKA semester 2
			 	 Print “Course chosen and total cost”
				  	 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “SD”
					 Prompt and get course/courses to be added #hash for SD courses AKA semester 2
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “NS”
					 Prompt and get course/courses to be added #hash for NS courses AKA semester 2
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “TS”
					 Prompt and get course/courses to be added #hash for TS courses AKA semester 2
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
				Else
				 Print “Each course will cost $587.89 because you are a non-international student”
				 Prompt and get Major
					If major == “CS”
					 Prompt and get course/courses to be added #hash for CS courses AKA semester 2
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “SD”
					 Prompt and get course/courses to be added #hash for SD courses AKA semester 2
		 	 	 Print “Course chosen and total cost”
			 	  	 Add course to hash and outfile courseReg
					 Add total cost to fees
				 	 Print “Your new total fees is $fees”
			 	 	 Change feestatus to unpaid
					Elseif major == “NS”
					 Prompt and get course/courses to be added #hash for NS courses AKA semester 2
			 	 Print “Course chosen and total cost”
			 	  	 Add course to hash and outfile courseReg
			 	 	 Add total cost to fees
					 Print “Your new total fees is $fees”
				 	 Change feestatus to unpaid
					Elseif major == “TS”
					 Prompt and get course/courses to be added #hash for TS courses AKA semester 2
				 Print “Course chosen and total cost”
			 	  	 Add course to hash and outfile courseReg
					 Add total cost to fees
			 	 	 Print “Your new total fees is $fees”
			 	 	 Change feestatus to unpaid
			Else
			 Print “You are not eligible to add a course because you are past the deadline”

			Elseif “Drop”
		 	 call subroutine for adddropWinter date check
			 	If date is before 2 weeks of start of term
			  	 Print “Eligible to drop a course”
					If internatStat == “international”
					  check hash and outfile courseReg for number of courses currently in hash
						If courseAmt >= 3
						  Print “Each course will cost $1396.3 because you are an international student”
						  Prompt and get course/courses to be dropped	#hash continuing courses and print out courses for sem2
				 		  Print “Course chosen and total cost”
					 	  Match stdin to hash in courseReg and remove
					 	  Deduct totalcost to fees
						  Print “Your new total fees is $fees”
						Else
					  	Print “You need at least 3 courses to continue either add or withdraw”

					Else
			 		  check hash and outfile courseReg for number of courses currently in hash
						If courseAmt >= 3
						  Print “Each course will cost $587.79 because you are an international student”
						  Prompt and get course/courses to be dropped	#hash continuing courses and print out courses for sem2
				 		  Print “Course chosen and total cost”
					 	  Match stdin to hash in courseReg and remove
					  	  Deduct totalcost to fees
						  Print “Your new total fees is $fees”
						Else
					  	Print “You need at least 3 courses to continue either add or withdraw”
				Else
			  	 Print “You are not eligible to drop a course because you are past the deadline”

			Elseif “Withdraw”
			  call subroutine for withdrawWinter date check
				If today is before Mar 25
				  Print “You are eligible to withdraw”
					If today is before 2 weeks of start of term 
				 	 Print “You are also eligible for a full refund”
					 Prompt and get confirmation of their withdrawal
				 	 Print “You will receive an email confirming your withdrawal”
					Elseif today is between progStartWinter and adddropWinter
					  Print “You will receive a 50% because you are past the first day of classes”
					  Prompt and get confirmation of their withdrawal
					  Print “You will receive an email confirming your withdrawal”
					Else
					Print “you are past the add/drop period so you are not eligible for a refund”
					Prompt and get confirmation of their withdrawal
					Print “You will receive an email confirming your withdrawal”
				Else
				Print “You are not eligible to withdraw because you are past the deadline		
			Else
			call subroutine to check feeDeadWin for CONTINUING students
				If today is before Jan 5
		 		Print “You are eligible to pay fees until Jan 5”
				Else
		  		Print “You are past the fee deadline, you will have to wait until next semester or when your program will permit		Elseif sem == 3
	Print “You are starting semester 3, if this is incorrect, exit the program and inform student services”
		If feestatus == “paid”
		Print “What do you choose to do:  Add/Drop/Withdraw/exit”
		While != “exit”
			If “add”
			 call subroutine for adddropFall date check
				If date is before 2 weeks of start of term
				 Print “Eligible to add a course/class”
					If internatStat == “international”
					  Print “Each course will cost $1396.3 because you are an international student”
					  Prompt and get Major
					  	If major == “CS”
					  	 Prompt and get course/courses to be added #hash for CS courses AKA semester 3
			 	 	 Print “Course chosen and total cost”
					   	 Add course to hash and outfile courseReg
					 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
				 	 	 Change feestatus to unpaid
						Elseif major == “SD”
						 Prompt and get course/courses to be added #hash for SD courses AKA semester 3
				  	 Print “Course chosen and total cost”
					  	 Add course to hash and outfile courseReg
				 	 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
				 	 	 Change feestatus to unpaid
						Elseif major == “NS”
						 Prompt and get course/courses to be added #hash for NS courses AKA semester 3
				 	 Print “Course chosen and total cost”
					   	 Add course to hash and outfile courseReg
					 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
					  	 Change feestatus to unpaid
						Elseif major == “TS”
						 Prompt and get course/courses to be added #hash for TS courses AKA semester 3
				  	 Print “Course chosen and total cost”
					  	 Add course to hash and outfile courseReg
				 	 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
				 	 	 Change feestatus to unpaid
					Else
					  Print “Each course will cost $587.89 because you are a non-international student”
					  Prompt and get Major
					  	If major == “CS”
					  	 Prompt and get course/courses to be added #hash for CS courses AKA semester 3
				 	 Print “Course chosen and total cost”
					  	 Add course to hash and outfile courseReg
					 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
				 	 	 Change feestatus to unpaid
						Elseif major == “SD”
						 Prompt and get course/courses to be added #hash for SD courses AKA semester 3
				 	 Print “Course chosen and total cost”
					   	 Add course to hash and outfile courseReg
					 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
				 	 	 Change feestatus to unpaid
						Elseif major == “NS”
						 Prompt and get course/courses to be added #hash for NS courses AKA semester 3
				 	 Print “Course chosen and total cost”
					  	 Add course to hash and outfile courseReg
				 	 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
				 	 	 Change feestatus to unpaid
						Elseif major == “TS”
						 Prompt and get course/courses to be added #hash for TS courses AKA semester 3
				 	 Print “Course chosen and total cost”
					   	 Add course to hash and outfile courseReg
					 	 Add total cost to fees
				 	 	 Print “Your new total fees is $fees”
				 	 	 Change feestatus to unpaid
				Else
				 Print “You are not eligible to add a course because you are past the deadline”

			Elseif “Drop”
		 	 call subroutine for adddropFall date check
				If date is before 2 weeks of start of term
			  	 Print “Eligible to drop a course”
					If internatStat == “international”
					 check hash and outfile courseReg for number of courses currently in hash
						If courseAmt >= 3
						  Print “Each course will cost $1396.3 because you are an international student”
						  Prompt and get course/courses to be dropped	#hash continuing courses and print out courses for sem2
					 	  Print “Course chosen and total cost”
					 	  Match stdin to hash in courseReg and remove
					 	  Deduct totalcost to fees
				 		  Print “Your new total fees is $fees”
						Else
					 	 Print “You need at least 3 courses to continue either add or withdraw”

					Else
				 	  check hash and outfile courseReg for number of courses currently in hash
						If courseAmt >= 3
						 Print “Each course will cost $587.9 because you are an international student”
						 Prompt and get course/courses to be dropped	#hash continuing courses and print out courses for sem2
					 	 Print “Course chosen and total cost”
					 	 Match stdin to hash in courseReg and remove
						 Deduct totalcost to fees
						 Print “Your new total fees is $fees”
						Else
						 Print “You need at least 3 courses to continue either add or withdraw”
				Else
				 Print “You are not eligible to drop a course because you are past the deadline”

			Elseif “Withdraw”
			  call subroutine for withdrawWinter date check
				If today is before Nov 12
				  Print “You are eligible to withdraw”
					If today is before 2 weeks of start of term 
				 	 Print “You are also eligible for a full refund”
					 Prompt and get confirmation of their withdrawal
				 	 Print “You will receive an email confirming your withdrawal”

					Elseif today is between progStartFall and adddropFall
					  Print “You will receive a 50% because you are past the first day of classes”
					  Prompt and get confirmation of their withdrawal
					  Print “You will receive an email confirming your withdrawal”
					Else
					Print “you are past the add/drop period so you are not eligible for a refund”
					Prompt and get confirmation of their withdrawal
					Print “You will receive an email confirming your withdrawal”
				Else
				Print “You are not eligible to withdraw because you are past the deadline”

		
			Else
			call subroutine to check feeDeadFall for CONTINUING students
				If today is before Sept 2
		 		Print “You are eligible to pay fees until Sept 2”
				Else
		  		Print “You are past the fee deadline, you will have to wait until next semester or when your program will permit"	
Elseif sem == 4
	 Print “You are starting semester 4, if this is incorrect, exit the program and inform student services”
		If feestatus == “paid”
		 Print “What do you choose to do:  Add/Drop/Withdraw/exit”
		 While != “exit”
			If “add”
		 	 call subroutine for adddropWinter date check
			 If date is before 2 weeks of start of term
			 Print “Eligible to add a course/class”
				If internatStat == “international”
				  Print “Each course will cost $1396.3 because you are an international student”
				  Prompt and get Major
				  	If major == “CS”
				 	 Prompt and get course/courses to be added #hash for CS courses AKA semester 4
			 	 Print “Course chosen and total cost”
				  	 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “SD”
					 Prompt and get course/courses to be added #hash for SD courses AKA semester 4
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “NS”
					 Prompt and get course/courses to be added #hash for NS courses AKA semester 4
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “TS”
					 Prompt and get course/courses to be added #hash for TS courses AKA semester 4
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
				Else
				 Print “Each course will cost $587.89 because you are a non-international student”
				 Prompt and get Major
					If major == “CS”
					 Prompt and get course/courses to be added #hash for CS courses AKA semester 4
				 Print “Course chosen and total cost”
					 Add course to hash and outfile courseReg
					 Add total cost to fees
					 Print “Your new total fees is $fees”
					 Change feestatus to unpaid
					Elseif major == “SD”
					 Prompt and get course/courses to be added #hash for SD courses AKA semester 4
		 	 	 Print “Course chosen and total cost”
			 	  	 Add course to hash and outfile courseReg
					 Add total cost to fees
				 	 Print “Your new total fees is $fees”
			 	 	 Change feestatus to unpaid
					Elseif major == “NS”
					 Prompt and get course/courses to be added #hash for NS courses AKA semester 4
			 	 Print “Course chosen and total cost”
			 	  	 Add course to hash and outfile courseReg
			 	 	 Add total cost to fees
					 Print “Your new total fees is $fees”
				 	 Change feestatus to unpaid
					Elseif major == “TS”
					 Prompt and get course/courses to be added #hash for TS courses AKA semester 4
				 Print “Course chosen and total cost”
			 	  	 Add course to hash and outfile courseReg
					 Add total cost to fees
			 	 	 Print “Your new total fees is $fees”
			 	 	 Change feestatus to unpaid
			Else
			 Print “You are not eligible to add a course because you are past the deadline”

			Elseif “Drop”
		 	 call subroutine for adddropWinter date check
			 	If date is before 2 weeks of start of term
			  	 Print “Eligible to drop a course”
					If internatStat == “international”
					  check hash and outfile courseReg for number of courses currently in hash
						If courseAmt >= 3
						  Print “Each course will cost $1396.3 because you are an international student”
						  Prompt and get course/courses to be dropped	#hash continuing courses and print out courses for sem2
				 		  Print “Course chosen and total cost”
					 	  Match stdin to hash in courseReg and remove
					 	  Deduct totalcost to fees
						  Print “Your new total fees is $fees”
						Else
					  	Print “You need at least 3 courses to continue either add or withdraw”

					Else
			 		  check hash and outfile courseReg for number of courses currently in hash
						If courseAmt >= 3
						  Print “Each course will cost $587.79 because you are an international student”
						  Prompt and get course/courses to be dropped	#hash continuing courses and print out courses for sem2
				 		  Print “Course chosen and total cost”
					 	  Match stdin to hash in courseReg and remove
					  	  Deduct totalcost to fees
						  Print “Your new total fees is $fees”
						Else
					  	Print “You need at least 3 courses to continue either add or withdraw”
				Else
			  	 Print “You are not eligible to drop a course because you are past the deadline”

			Elseif “Withdraw”
			  call subroutine for withdrawWinter date check
				If today is before Mar 25
				  Print “You are eligible to withdraw”
					If today is before 2 weeks of start of term 
				 	 Print “You are also eligible for a full refund”
					 Prompt and get confirmation of their withdrawal
				 	 Print “You will receive an email confirming your withdrawal”
					Elseif today is between progStartWinter and adddropWinter
					  Print “You will receive a 50% because you are past the first day of classes”
					  Prompt and get confirmation of their withdrawal
					  Print “You will receive an email confirming your withdrawal”
					Else
					Print “you are past the add/drop period so you are not eligible for a refund”
					Prompt and get confirmation of their withdrawal
					Print “You will receive an email confirming your withdrawal”
				Else
				Print “You are not eligible to withdraw because you are past the deadline		
			Else
			call subroutine to check feeDeadWin for CONTINUING students
				If today is before Jan 5
		 		Print “You are eligible to pay fees until Jan 5”
				Else
		  		Print “You are past the fee deadline, you will have to wait until next semester or when your program will permit	
	
	Else 
	Print “Invalid”

Elseif studentstatus == “expelled”
Print “You are not eligible to use this program because you are expelled, please choose exit to exit the program”
Else
“Invalid”




	


