#!/usr/bin/perl

#Created by : Jesse Dela Rosa & Jervhee Dacio
#filename : checkICT.pl
#
#Functionality: The admin will enter the student id, the program will then print out the #information assigned on the ID. Based on the info given out the admin then chooses whether #to add, drop or withdraw. The program then finds out if the admin can do the specified #actions based on dates, student status, fee status etc. Choosing add or drop prints out the #results on an outfile, while withdrawing changes your student status to withdrew.
#INPUT: $idtocheck, $opt, $courseDrop, $newCourse
#OUTPUT: %checkhash, %sem2, %sem3, %sem4, %sem1, changes to courseReg file and mainFile.txt
#
#Constants:
#$noCourse = "X";
#$with = "Withdrew";
#$addedFee = 0;
#$tuitionInt = 6981.75;
#$tuitionReg = 5870;
#$courseFeeInt = 1396.30;
#$courseFeeReg = 587.89;
#@feeNew = (0, 0, 0, 5, 7, 2015);
#@feeFall = (0, 0, 0, 2, 8, 2015);
#@feeWin = (0, 0, 0, 5, 0, 2016);
#@addropFallStart = (0, 0, 0, 2, 8, 2015);
#@addropFallEnd = (0, 0, 0, 12, 8, 2015);
#@addropWinterStart = (0, 0, 0, 5, 0, 2016);
#@addropWinterEnd = (0, 0, 0, 16, 0, 201);
#@withdrawFallStart = (0, 0, 0, 2, 10, 2015);
#@withdrawFallEnd = (0, 0, 0, 12, 10, 2015);
#@withdrawWinStart = (0, 0, 0, 5, 0, 2016);
#@withdrawWinEnd = (0, 0, 0, 25, 2, 2016);

#Formulas:
#$untilDayStart = (($addropFallStart - $time) / 86400),
#$untilDayEnd = (($addropFallEnd - $time) / 86400),
#$untilDayWin = (($feeWin - $time) / 86400),
#$untilDayFall = (($feeFall - $time) / 86400),
#$untilDayNew = (($feeNew - $time) / 86400),
#$untilDayStart = (($addropWinterStart - $time) / 86400),
#$untilDayEnd = (($addropWinterEnd - $time) / 86400),
#$untilWithdrawFallStart = (($withdrawFallStart - $time) / 86400),
#$untilWithdrawFallEnd = (($withdrawFallEnd - $time) / 86400),
#$untilWithdrawWinStart = (($withdrawWinStart - $time) / 86400),
#$untilWithdrawWinEnd = (($withdrawWinEnd - $time) / 86400)
#Sources used : Diana Pettit
#				http://perlmeme.org/faqs/datetime/comparing_dates.html
#				Hagi Elghahagi – March 17, 2011
#				https://askubuntu.com/questions/434051/how-to-replace-a-string-on-the-5th-line-of-multiple-text-files
#				http://search.cpan.org/~drolsky/Time-Local-1.2300/lib/Time/Local.pm
# 				http://search.cpan.org/~stbey/Carp-Clan-6.04/lib/Carp/Clan.pod
#				http://search.cpan.org/~stbey/Date-Calc-6.4/lib/Date/Calc.pod

use strict;
use warnings;

my (
    $studid,   $fname,      $lname,      $school,       $prog,
    $major,    $sem,        $studStat,   $feeStat,      $intStat,
    $check,    @check,      %checkhash,  $checkhash,    $idtocheck,
    $flag,     $date,       $now,        @now,          $opt,
    $addedFee, $tuitionInt, $tuitionReg, $courseFeeInt, $courseFeeReg
);    # main program variables

$addedFee     = 0;
$tuitionInt   = 6981.75;
$tuitionReg   = 5870;
$courseFeeInt = 1396.30;
$courseFeeReg = 587.89;

my (
    $newCourse, $courseDrop,      $aryCourseIndex, $aryIndex,
    @aryCourse, $lengthAryCourse, $lengthAry,      $noCourse,
    @aryLine,   $aryLine,         $aryCourse,      @aryNew,
    $aryNew,    $aryEndIndex
);    # add and drop course subroutine variables

$aryNew      = "";
$aryCourse   = "";
$aryEndIndex = 0;
$noCourse    = "X";
$aryIndex    = -1;

my ( $with, @aryMain, $lengthAryMain, $aryMain, $aryMainIndex, $aryNewStat,
    @aryNewStat );    # variables for withdrawStudent subroutine

$with = "Withdrew";

my (
    @today,           #allow subroutines to share these variables
    $time,
    $untilDayStart,
    $untilDayEnd,
    $untilHour
);

my (
    @feeNew,          #feeNew subroutine variables
    $feeNew,
    $untilDayNew,
    $untilHourNew
);

my (
    @feeFall,         #feeFall subroutine variables
    $feeFall,
    $untilDayFall,
    $untilHourFall
);

my (
    @feeWin,          #feeWin subroutine variables
    $feeWin,
    $untilDayWin,
    $untilHourWin
);

my (
    @addropFallStart,    #addropFall subroutine variables
    @addropFallEnd,
    $addropFallStart,
    $addropFallEnd
);

my (
    @addropWinterStart,    #adddropWin subroutine variables
    @addropWinterEnd,
    $addropWinterStart,
    $addropWinterEnd
);

my (
    @withdrawFallStart,    #withdrawFall subroutine variables
    @withdrawFallEnd,
    $withdrawFallStart,
    $withdrawFallEnd,
    $untilWithdrawFallStart,
    $untilWithdrawFallEnd
);

my (
    @withdrawWinStart,     #withdrawWin subroutine variables
    @withdrawWinEnd,
    $withdrawWinStart,
    $withdrawWinEnd,
    $untilWithdrawWinStart,
    $untilWithdrawWinEnd
);

my (
    $sem1course1, $sem1course2, $sem1course3, $sem1course4, $sem1course5,
    %sem1hash,    $sem1hash,    @sem1,        $sem1
);
my (
    $sem2course1, $sem2course2, $sem2course3, $sem2course4, $sem2course5,
    %sem2hash,    $sem2hash,    @sem2,        $sem2
);
my (
    $sem3course1, $sem3course2, $sem3course3, $sem3course4, $sem3course5,
    %sem3hash,    $sem3hash,    $sem3,        @sem3
);
my (
    $sem4course1, $sem4course2, $sem4course3, $sem4course4, $sem4course5,
    %sem4hash,    $sem4hash,    @sem4,        $sem4
);

#############################################################################################
# SUBROUTINE for getting current DATE
#############################################################################################
sub currentDate {
    open( OUTFILE, ">currDate.rpt" ) || die "$! on line $.\n";
    $now = `date`;    #backticks

    @now = split / /, $now;    #extract variable date on spaces
    if ( $now[2] eq "" )       #if statement to check if day is one number
                               #and one space or two numbers
    {
        @now = ( $now[1], $now[3], $now[6] )
          ;                    #2nd, 4th, and 7th nums from the system date
    }
    else {
        @now = ( $now[1], $now[2], $now[5] )
          ;                    #2nd, 3rd, and 6th nums from the system date
    }
    chomp( $date = join " ", @now );
    print OUTFILE "$date\n";

    print "Today's date is : ";
    system( "cat", "currDate.rpt" );
}

#############################################################################################
# SUBROUTINE for checking fee deadline date FOR SEMESTER 1/NEW students
#############################################################################################
sub feeNew {
    use Time::Local;

    @today = localtime();
    $time  = timelocal(@today);

    @feeNew = ( 0, 0, 0, 5, 7, 2015 )
      ; #(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
    $feeNew = timelocal(@feeNew);

    $untilDayNew = ( ( $feeNew - $time ) / 86400 );

}

#############################################################################################
# SUBROUTINE for checking fee deadline date FOR FALL SEMESTER students
#############################################################################################
sub feeFall {
    use Time::Local;

    @today = localtime();
    $time  = timelocal(@today);

    @feeFall = ( 0, 0, 0, 2, 8, 2015 )
      ; #(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
    $feeFall = timelocal(@feeFall);

    $untilDayFall = ( ( $feeFall - $time ) / 86400 );

}

#############################################################################################
# SUBROUTINE for checking fee deadline date FOR WINTER SEMESTER students
#############################################################################################
sub feeWin {
    use Time::Local;

    @today = localtime();
    $time  = timelocal(@today);

    @feeWin = ( 0, 0, 0, 5, 0, 2016 )
      ; #(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
    $feeWin = timelocal(@feeWin);

    $untilDayWin = ( ( $feeWin - $time ) / 86400 );
}
#############################################################################################
# SUBROUTINE for checking add/drop deadline date FOR SEM 1/NEW students or SEM 3/CONT students
#############################################################################################
sub addropFall {
    use Time::Local;

    @today = localtime();
    $time  = timelocal(@today);

#@addropFallStart = (0, 0, 0, 2, 8, 2015);	#(S, MIN, HR, DAY, MONTH, YEAR) THe month goes from 0-11 because that is the number of months until January
#@addropFallEnd = (0, 0, 0, 12, 8, 2015);

    # date for testing functionality of addropFall
    @addropFallStart = ( 0, 0, 0, 16, 3, 2015 );
    @addropFallEnd   = ( 0, 0, 0, 25, 3, 2015 );

    $addropFallStart = timelocal(@addropFallStart);
    $addropFallEnd   = timelocal(@addropFallEnd);

    $untilDayStart = ( ( $addropFallStart - $time ) / 86400 );

    $untilDayEnd = ( ( $addropFallEnd - $time ) / 86400 );

}
#############################################################################################
# SUBROUTINE for checking add/drop deadline date FOR SEM 2 and 4 CONT students
#############################################################################################
sub addropWin {
    use Time::Local;

    @today = localtime();
    $time  = timelocal(@today);

    @addropWinterStart = ( 0, 0, 0, 5, 0, 2016 )
      ; #(S, MIN, HR, DAY, MONTH, YEAR) THe month goes from 0-11 because that is the number of months until January (5 dec 2016)
    @addropWinterEnd = ( 0, 0, 0, 16, 0, 2016 );

    $addropWinterStart = timelocal(@addropWinterStart);
    $addropWinterEnd   = timelocal(@addropWinterEnd);

    $untilDayStart = ( ( $addropWinterStart - $time ) / 86400 );

    $untilDayEnd = ( ( $addropWinterEnd - $time ) / 86400 );

}
#############################################################################################
# SUBROUTINE for checking withdrawal deadline date FOR SEM 1 and 3 students
#############################################################################################
sub withdrawFall {
    use Time::Local;

    @today = localtime();
    $time  = timelocal(@today);

    @withdrawFallStart = ( 0, 0, 0, 2, 10, 2015 )
      ; #(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
    @withdrawFallEnd = ( 0, 0, 0, 12, 10, 2015 );

    $withdrawFallStart = timelocal(@withdrawFallStart);
    $withdrawFallEnd   = timelocal(@withdrawFallEnd);

    $untilWithdrawFallStart = ( ( $withdrawFallStart - $time ) / 86400 );
    $untilWithdrawFallEnd   = ( ( $withdrawFallEnd - $time ) / 86400 );

}

#############################################################################################
# SUBROUTINE for checking withdrawal deadline date FOR SEM 1 and 3 students
#############################################################################################
sub withdrawWin {
    use Time::Local;

    @today = localtime();
    $time  = timelocal(@today);

    @withdrawWinStart = ( 0, 0, 0, 5, 0, 2016 )
      ; #(S, MIN, HR, DAY, MONTH, YEAR) THe month goes 								#from 0-11 because that is the number of months until January
    @withdrawWinEnd = ( 0, 0, 0, 25, 2, 2016 );

    $withdrawWinStart = timelocal(@withdrawWinStart);
    $withdrawWinEnd   = timelocal(@withdrawWinEnd);

    $untilWithdrawWinStart = ( ( $withdrawWinStart - $time ) / 86400 );
    $untilWithdrawWinEnd   = ( ( $withdrawWinEnd - $time ) / 86400 );
}

###################################################################################################################
#	ADDING A COURSE TO COURSEREG
###################################################################################################################
sub addCourse {
    open( FH, "courseReg.txt" ) || die "Can't read file: $!\n";
    chomp( @aryLine = <FH> );
    close(FH);
	
    print "Enter course to add: \n";
    chomp( $newCourse = <STDIN> );

    foreach $aryLine (
        @aryLine)    # array using each line in the file as an element

    {
        @aryCourse = split( /\s/, $aryLine );   #split each line by a whitespace

        $aryIndex++;

        $lengthAryCourse = @aryCourse;

        foreach (@aryCourse
          )    #for each element in each line which is separated by a space
        {

            if (/$idtocheck/i
              )    #if the student ID entered matches the element on the line
            {

                for (
                    $aryCourseIndex = 0 ;
                    $aryCourseIndex < $lengthAryCourse ;
                    $aryCourseIndex++
                  )    # iterate through each element
                {

                    if ( $aryCourse[$aryCourseIndex] eq $newCourse ) {
                        print
                          "$newCourse is already registered for the student.\n";
                        last;
                    }
                    elsif ( $aryCourse[$aryCourseIndex] eq
                        $noCourse )    # if an unregistered course is found
                    {

                        $aryEndIndex = $aryIndex;
                        splice( @aryCourse, $aryCourseIndex, 1, $newCourse )
                          ; #splice and replace the unregistered course with newCourse

                        $aryNew = join( ' ', @aryCourse );

                        splice( @aryLine, $aryEndIndex, 1, $aryNew );

                        foreach $aryLine (@aryLine
                          )    #foreach to reprint new array values into file
                        {

                            @aryNew = split( /\s/, $aryLine );

                            open( FH, "+<courseReg.txt" ) 
                              || die "Can't open file for read/write: $! $.\n";
                            system(
                                `sed -i "s/$aryCourse/$aryNew/" courseReg.txt`);
                            close(FH);

                            $addedFee += $courseFeeReg;
                            printf
"A new charge of \$%.2f will be displayed on student's mysait.ca financial information section.\n",
                              $addedFee;
                            exit 1;

                        }

                    }

                    else {
                        if ( $aryCourseIndex > 0 ) {
                            print
"$aryCourse[$aryCourseIndex] is currently registered for the student.\n";
                        }
                        $aryCourse = join( ' ', @aryCourse );
                    }

                }

            }
        }

    }
    exit 1;
}    #end sub addCourse

###################################################################################################################
#	DROPPING A COURSE TO COURSEREG
###################################################################################################################
sub dropCourse {

    open( FH, "courseReg.txt" ) || die "Can't read file: $!\n";
    chomp( @aryLine = <FH> );
    close(FH);

    print "Enter course to drop: \n";
    chomp( $courseDrop = <STDIN> );

    foreach $aryLine (
        @aryLine)    # array using each line in the file as an element

    {
        @aryCourse = split( /\s/, $aryLine );   #split each line by a whitespace

        $aryIndex++;

        $lengthAryCourse = @aryCourse;

        foreach (@aryCourse
          )    #for each element in each line which is separated by a space
        {
            if (/$idtocheck/i
              )    #if the student ID entered matches the element on the line
            {

                for (
                    $aryCourseIndex = 0 ;
                    $aryCourseIndex < $lengthAryCourse ;
                    $aryCourseIndex++
                  )    # iterate through each element
                {

                    if ( $aryCourse[$aryCourseIndex] eq $courseDrop
                      )   # if a registered course matches chosen course to drop
                    {

                        $aryEndIndex = $aryIndex;
                        splice( @aryCourse, $aryCourseIndex, 1, $noCourse )
                          ; #splice and replace the registered course with noCourse, which is a capital X

                        $aryNew = join( ' ', @aryCourse );

                    }
                    else {
                        if ( $aryCourseIndex > 0 ) {
                            print
"$aryCourse[$aryCourseIndex] is currently registered for the student.\n";
                        }
                        $aryCourse = join( ' ', @aryCourse );

                    }

                }
            }
        }

    }

    splice( @aryLine, $aryEndIndex, 1, $aryNew );

    foreach $aryLine (@aryLine)   #foreach to reprint new array values into file
    {

        @aryNew = split( /\s/, $aryLine );

        open( FH, "+<courseReg.txt" )
          || die "Can't open file for read/write: $!\n";
        system(`sed -i "s/$aryCourse/$aryNew/" courseReg.txt`);
        close(FH);
    }
    exit 1;
}    #end sub dropCourse

###################################################################################
#	withdrawStudent subroutine	#changes SSTAT to Withdrew
###################################################################################
sub withdrawStudent {

    open( FH, "mainFile.txt" ) || die "Can't read file: $!\n";
    chomp( @aryLine = <FH> );
    close(FH);

    foreach $aryLine (
        @aryLine)    # array using each line in the file as an element

    {
        @aryMain = split( /\s/, $aryLine );    #split each line by a whitespace

        $aryIndex++;

        $lengthAryMain = @aryMain;

        foreach (@aryMain
          )    #for each element in each line which is separated by a space
        {
            if (/$idtocheck/i
              )    #if the student ID entered matches the element on the line
            {

                for (
                    $aryMainIndex = 0 ;
                    $aryMainIndex < $lengthAryMain ;
                    $aryMainIndex++
                  )    # iterate through each element
                {

                    if ( $aryMain[$aryMainIndex] =~ /new/i || $aryMain[$aryMainIndex] =~ /continuing/i
                      )   # if a registered course matches chosen course to drop
                    {

                        $aryEndIndex = $aryIndex;
                        splice( @aryMain, $aryMainIndex, 1, $with )
                          ; #splice and replace the registered course with noCourse, which is a capital X

                        $aryNewStat = join( ' ', @aryMain );
                        last;
                    }
                    else {

                        $aryMain = join( ' ', @aryMain );

                    }

                }
            }
        }

    }

    splice( @aryLine, $aryEndIndex, 1, $aryNewStat );

    foreach $aryLine (@aryLine)   #foreach to reprint new array values into file
    {

        @aryNewStat = split( /\s/, $aryLine );

        open( FH, "+<mainFile.txt" )
          || die "Can't open file for read/write: $!\n";
        system(`sed -i "s/$aryMain/$aryNewStat/" mainFile.txt`);
        close(FH);
    }
    exit 1;

}
##################################################################################################################
#	SUBROUTINE for printing semester 1 courses
##################################################################################################################
sub prgMajandCourse1 {

    open( MYFILE, "sem1course.txt" );
    chop( @sem1 = <MYFILE> );
    close(MYFILE);
    $flag = 1;

    foreach $sem1 (@sem1) {
        ( $sem1course1, $sem1course2, $sem1course3, $sem1course4, $sem1course5 )
          = split( / /, $sem1 );
        %sem1hash = ();
        $sem1hash = {
            COURSE1 => $sem1course1,
            COURSE2 => $sem1course2,
            COURSE3 => $sem1course3,
            COURSE4 => $sem1course4,
            COURSE5 => $sem1course5,
        };
    }    #end foreach
    print "Here are the available Courses\n";
    printf "%s\n", $sem1hash->{COURSE1};
    printf "%s\n", $sem1hash->{COURSE2};
    printf "%s\n", $sem1hash->{COURSE3};
    printf "%s\n", $sem1hash->{COURSE4};
    printf "%s\n", $sem1hash->{COURSE5};

}
#############################################################################################
# SUBROUTINE for Prompting and getting Major and Course (SEMESTER 3)
#############################################################################################
sub prgMajandCourse3 {

    if ( "CS" eq $checkhash->{MAJ} ) {

        open( MYFILE, "sem3courseCS.txt" );
        chop( @sem3 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem3 (@sem3) {
            (
                $sem3course1, $sem3course2, $sem3course3,
                $sem3course4, $sem3course5
            ) = split( / /, $sem3 );
            %sem3hash = ();
            $sem3hash = {
                COURSE1 => $sem3course1,
                COURSE2 => $sem3course2,
                COURSE3 => $sem3course3,
                COURSE4 => $sem3course4,
                COURSE5 => $sem3course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem3hash->{COURSE1};
        printf "%s\n", $sem3hash->{COURSE2};
        printf "%s\n", $sem3hash->{COURSE3};
        printf "%s\n", $sem3hash->{COURSE4};
        printf "%s\n", $sem3hash->{COURSE5};
    }
    elsif ( "SD" eq $sem3hash->{MAJ} ) {

        open( MYFILE, "sem3courseSD.txt" );
        chop( @sem3 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem3 (@sem3) {
            (
                $sem3course1, $sem3course2, $sem3course3,
                $sem3course4, $sem3course5
            ) = split( / /, $sem3 );
            %sem3hash = ();
            $sem3hash = {
                COURSE1 => $sem3course1,
                COURSE2 => $sem3course2,
                COURSE3 => $sem3course3,
                COURSE4 => $sem3course4,
                COURSE5 => $sem3course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem3hash->{COURSE1};
        printf "%s\n", $sem3hash->{COURSE2};
        printf "%s\n", $sem3hash->{COURSE3};
        printf "%s\n", $sem3hash->{COURSE4};
        printf "%s\n", $sem3hash->{COURSE5};
    }
    elsif ( "NS" eq $sem3hash->{MAJ} ) {

        open( MYFILE, "sem3courseNS.txt" );
        chop( @sem3 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem3 (@sem3) {
            (
                $sem3course1, $sem3course2, $sem3course3,
                $sem3course4, $sem3course5
            ) = split( / /, $sem3 );
            %sem3hash = ();
            $sem3hash = {
                COURSE1 => $sem3course1,
                COURSE2 => $sem3course2,
                COURSE3 => $sem3course3,
                COURSE4 => $sem3course4,
                COURSE5 => $sem3course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem3hash->{COURSE1};
        printf "%s\n", $sem3hash->{COURSE2};
        printf "%s\n", $sem3hash->{COURSE3};
        printf "%s\n", $sem3hash->{COURSE4};
        printf "%s\n", $sem3hash->{COURSE5};
    }
    else {

        open( MYFILE, "sem3courseTelecom.txt" );
        chop( @sem3 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem3 (@sem3) {
            (
                $sem3course1, $sem3course2, $sem3course3,
                $sem3course4, $sem3course5
            ) = split( / /, $sem3 );
            %sem3hash = ();
            $sem3hash = {
                COURSE1 => $sem3course1,
                COURSE2 => $sem3course2,
                COURSE3 => $sem3course3,
                COURSE4 => $sem3course4,
                COURSE5 => $sem3course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem3hash->{COURSE1};
        printf "%s\n", $sem3hash->{COURSE2};
        printf "%s\n", $sem3hash->{COURSE3};
        printf "%s\n", $sem3hash->{COURSE4};
        printf "%s\n", $sem3hash->{COURSE5};
    }

}

#############################################################################################
# SUBROUTINE for Prompting and getting Major and Course (SEMESTER 2)
#############################################################################################
sub prgMajandCourse2 {

    if ( "CS" eq $checkhash->{MAJ} ) {

        open( MYFILE, "sem2courseCS.txt" );
        chop( @sem2 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem2 (@sem2) {
            (
                $sem2course1, $sem2course2, $sem2course3,
                $sem2course4, $sem2course5
            ) = split( / /, $sem2 );
            %sem2hash = ();
            $sem2hash = {
                COURSE1 => $sem2course1,
                COURSE2 => $sem2course2,
                COURSE3 => $sem2course3,
                COURSE4 => $sem2course4,
                COURSE5 => $sem2course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem2hash->{COURSE1};
        printf "%s\n", $sem2hash->{COURSE2};
        printf "%s\n", $sem2hash->{COURSE3};
        printf "%s\n", $sem2hash->{COURSE4};
        printf "%s\n", $sem2hash->{COURSE5};
    }
    elsif ( "SD" eq $sem2hash->{MAJ} ) {
        open( MYFILE, "sem2courseSD.txt" );
        chop( @sem2 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem2 (@sem2) {
            (
                $sem2course1, $sem2course2, $sem2course3,
                $sem2course4, $sem2course5
            ) = split( / /, $sem2 );
            %sem2hash = ();
            $sem2hash = {
                COURSE1 => $sem2course1,
                COURSE2 => $sem2course2,
                COURSE3 => $sem2course3,
                COURSE4 => $sem2course4,
                COURSE5 => $sem2course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem2hash->{COURSE1};
        printf "%s\n", $sem2hash->{COURSE2};
        printf "%s\n", $sem2hash->{COURSE3};
        printf "%s\n", $sem2hash->{COURSE4};
        printf "%s\n", $sem2hash->{COURSE5};
    }
    elsif ( "NS" eq $checkhash->{MAJ} ) {

        open( MYFILE, "sem2courseNS.txt" );
        chop( @sem2 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem2 (@sem2) {
            (
                $sem2course1, $sem2course2, $sem2course3,
                $sem2course4, $sem2course5
            ) = split( / /, $sem2 );
            %sem2hash = ();
            $sem2hash = {
                COURSE1 => $sem2course1,
                COURSE2 => $sem2course2,
                COURSE3 => $sem2course3,
                COURSE4 => $sem2course4,
                COURSE5 => $sem2course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem2hash->{COURSE1};
        printf "%s\n", $sem2hash->{COURSE2};
        printf "%s\n", $sem2hash->{COURSE3};
        printf "%s\n", $sem2hash->{COURSE4};
        printf "%s\n", $sem2hash->{COURSE5};
    }
    else {

        open( MYFILE, "sem2courseTelecom.txt" );
        chop( @sem2 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem2 (@sem2) {
            (
                $sem2course1, $sem2course2, $sem2course3,
                $sem2course4, $sem2course5
            ) = split( / /, $sem2 );
            %sem2hash = ();
            $sem2hash = {
                COURSE1 => $sem2course1,
                COURSE2 => $sem2course2,
                COURSE3 => $sem2course3,
                COURSE4 => $sem2course4,
                COURSE5 => $sem2course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem2hash->{COURSE1};
        printf "%s\n", $sem2hash->{COURSE2};
        printf "%s\n", $sem2hash->{COURSE3};
        printf "%s\n", $sem2hash->{COURSE4};
        printf "%s\n", $sem2hash->{COURSE5};
    }

}

#############################################################################################
# SUBROUTINE for Prompting and getting Major and Course (SEMESTER 4)
#############################################################################################
sub prgMajandCourse4 {

    if ( "CS" eq $checkhash->{MAJ} ) {

        open( MYFILE, "sem4courseCS.txt" );
        chop( @sem4 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem4 (@sem4) {
            (
                $sem4course1, $sem4course2, $sem4course3,
                $sem4course4, $sem4course5, %sem4hash,
                $sem4hash,    @sem4,        $sem4
            ) = split( / /, $sem4 );
            %sem4hash = ();
            $sem4hash = {
                COURSE1 => $sem4course1,
                COURSE2 => $sem4course2,
                COURSE3 => $sem4course3,
                COURSE4 => $sem4course4,
                COURSE5 => $sem4course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem4hash->{COURSE1};
        printf "%s\n", $sem4hash->{COURSE2};
        printf "%s\n", $sem4hash->{COURSE3};
        printf "%s\n", $sem4hash->{COURSE4};
        printf "%s\n", $sem4hash->{COURSE5};
    }
    elsif ( "SD" eq $sem4hash->{MAJ} ) {

        open( MYFILE, "sem4courseSD.txt" );
        chop( @sem4 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem4 (@sem4) {
            (
                $sem4course1, $sem4course2, $sem4course3,
                $sem4course4, $sem4course5, %sem4hash,
                $sem4hash,    @sem4,        $sem4
            ) = split( / /, $sem4 );
            %sem4hash = ();
            $sem4hash = {
                COURSE1 => $sem4course1,
                COURSE2 => $sem4course2,
                COURSE3 => $sem4course3,
                COURSE4 => $sem4course4,
                COURSE5 => $sem4course5,
            };
        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem4hash->{COURSE1};
        printf "%s\n", $sem4hash->{COURSE2};
        printf "%s\n", $sem4hash->{COURSE3};
        printf "%s\n", $sem4hash->{COURSE4};
        printf "%s\n", $sem4hash->{COURSE5};
    }
    elsif ( "NS" eq $checkhash->{MAJ} ) {

        open( MYFILE, "sem4courseNS.txt" );
        chop( @sem4 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem4 (@sem4) {
            (
                $sem4course1, $sem4course2, $sem4course3,
                $sem4course4, $sem4course5, %sem4hash,
                $sem4hash,    @sem4,        $sem4
            ) = split( / /, $sem4 );
            %sem4hash = ();
            $sem4hash = {
                COURSE1 => $sem4course1,
                COURSE2 => $sem4course2,
                COURSE3 => $sem4course3,
                COURSE4 => $sem4course4,
                COURSE5 => $sem4course5,
            };

        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem4hash->{COURSE1};
        printf "%s\n", $sem4hash->{COURSE2};
        printf "%s\n", $sem4hash->{COURSE3};
        printf "%s\n", $sem4hash->{COURSE4};
        printf "%s\n", $sem4hash->{COURSE5};
    }
    else {

        open( MYFILE, "sem4courseTelecom.txt" );
        chop( @sem4 = <MYFILE> );
        close(MYFILE);
        $flag = 1;

        foreach $sem4 (@sem4) {
            (
                $sem4course1, $sem4course2, $sem4course3,
                $sem4course4, $sem4course5, %sem4hash,
                $sem4hash,    @sem4,        $sem4
            ) = split( / /, $sem4 );
            %sem4hash = ();
            $sem4hash = {
                COURSE1 => $sem4course1,
                COURSE2 => $sem4course2,
                COURSE3 => $sem4course3,
                COURSE4 => $sem4course4,
                COURSE5 => $sem4course5,
            };

        }    #end foreach
        print "Here are the available Courses\n";
        printf "%s\n", $sem4hash->{COURSE1};
        printf "%s\n", $sem4hash->{COURSE2};
        printf "%s\n", $sem4hash->{COURSE3};
        printf "%s\n", $sem4hash->{COURSE4};
        printf "%s\n", $sem4hash->{COURSE5};
    }

}

#############################################################################################
# MAIN PROGRAM #
#############################################################################################

open( MYFILE, "mainFile.txt" );
chop( @check = <MYFILE> );
close(MYFILE);
$flag = 1;

&currentDate;
print "Enter the ID of the student wishing to check fees: ";
chomp( $idtocheck = <STDIN> );

foreach $check (@check) {
    (
        $studid, $fname, $lname,    $school,  $prog,
        $major,  $sem,   $studStat, $feeStat, $intStat
    ) = split( / /, $check );
    %checkhash = ();
    $checkhash = {
        ID     => $studid,
        FNAME  => $fname,
        LNAME  => $lname,
        SCHOOL => $school,
        PROG   => $prog,
        MAJ    => $major,
        SEM    => $sem,
        SSTAT  => $studStat,
        FSTAT  => $feeStat,
        ISTAT  => $intStat

    };

    if ( $idtocheck eq $checkhash->{ID} ) {

        printf
"Student ID : %27s\nFirst Name : %27s\nLast Name : %28s\nSchool : %31s\nProgram : %30s\nMajor : %32s\nSemester : %29s\nStudent Status : %23s\nFee Status : %27s\nInternational Status : %17s\n\n",
          $checkhash->{ID},     $checkhash->{FNAME}, $checkhash->{LNAME},
          $checkhash->{SCHOOL}, $checkhash->{PROG},  $checkhash->{MAJ},
          $checkhash->{SEM},    $checkhash->{SSTAT}, $checkhash->{FSTAT},
          $checkhash->{ISTAT};

        $flag = 0;

        if ( "New" eq $checkhash->{SSTAT} ) {
            if ( "Paid" eq $checkhash->{FSTAT} ) {
                print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
                $opt = "null";
                chomp( $opt = <STDIN> );

                while ( $opt !~ /exit/i )   #Begin while loop for menu selection
                {
                    if ( $opt =~ /Add/i ) {
                        &addropFall;        #check add/drop date for fall
                        if ( $untilDayStart >
                            1 ) # add/drop period has not begun yet for fall sem
                        {
                            printf
"Student has to wait until September 2 of this year to add courses. There is currently %0.f day(s) until that start date for adding or dropping courses.\n",
                              $untilDayStart;
                            last;
                        }
                        elsif ($untilDayEnd >= 1
                            && $untilDayEnd <=
                            10 )    # Add/drop period for fall begins
                        {
                            print "Student is eligible to add a course.\n";
                            printf
"Student has until September 12 of this year to add or drop classes. There is currently %0.f day(s) until the deadline\n",
                              $untilDayEnd;
                            if ( "International" eq $checkhash->{ISTAT} ) {
                                print
"Each course will cost \$1396.30 because student is  international.\n";
						
                                &prgMajandCourse1;
                                &addCourse;
                            }
                            else {
                                print
"Each course will cost \$587.89 because student is non-international.\n";
                                &prgMajandCourse1;
                                &addCourse;
                            }
                        }
                        elsif ( $untilDayEnd < 1 ) {
                            $untilDayEnd =
                              ( ( $addropFallEnd - $time ) / 3600 );
                            printf
"There is %.0f hour(s) until the add/drop deadline for fall semester students. \n",
                              $untilDayEnd;
                            if ( "International" eq $checkhash->{ISTAT} ) {
                                print
"Each course will cost \$1396.30 because student is  international.\n";
                                &prgMajandCourse1;
                                &addCourse;

                            }
                            else {
                                print
"Each course will cost \$587.89 because student is non-international.\n";
                                &prgMajandCourse1;
                                &addCourse;
                            }
                        }
                        else {
                            print
"The add/drop period has expired. Action can no longer be performed.\n";
                            last;
                        }
                    }    #end if for adding courses
                    elsif ( $opt =~ /Drop/i ) {
                        &addropFall;    #check add/drop date for fall
                        if ( $untilDayStart > 1
                          )    # Cannot drop course until add/drop period begins
                        {

                            printf
"Student has to wait until September 2 of this year to drop courses. There is currently %0.f day(s) until that start date for dropping courses.\n",
                              $untilDayStart;
                            print
"Select a task for perform: Add/Drop/Withdraw/Exit\n";
                            chomp( $opt = <STDIN> );
                            printf
"Student has to wait until September 2 of this year to drop courses. There is currently %0.f day(s) until that start date for dropping courses.\n",
                              $untilDayStart;
                            &prgMajandCourse1;
                            &dropCourse;

                        }
                        elsif ($untilDayEnd >= 1
                            && $untilDayEnd <= 10 )    #add/drop period begins
                        {
                            printf
"Student has until September 12 of this year to drop classes. There is currently %0.f day(s) until the deadline\n",
                              $untilDayEnd;
                            &prgMajandCourse1;
                            &dropCourse;

                        }
                        else                           # add/drop period expires
                        {
                            print
"The add/drop period has expired. Action can no longer be performed.\n";
                            last;
                        }

                    }    #end if for dropping courses
                    elsif ( $opt =~ /withdraw/i ) {
                        &withdrawFall;
                        if ( $untilWithdrawFallStart >
                            1 )    # if before program start, full refund
                        {
                            printf
"This person's program has not started yet, therefore he/she is eligible for a FULL refund. There is currently %.0f days until the student's program starts\n",
                              $untilWithdrawFallStart;
                            &withdrawStudent;    #	change SSTAT to withdrew

                        }
                        elsif ($untilWithdrawFallEnd >= 1
                            && $untilWithdrawFallEnd <=
                            71 )    # elsif before withdraw date, half refund
                        {
                            printf
"This person's program has already started, therefore he/she is eligible for HALF refund. There is currently %.0f days until your Withdraw deadline refund policy ends\n",
                              $untilWithdrawFallEnd;
                            &withdrawStudent;    #	change SSTAT to withdrew

                        }
                        else                     # else no refund
                        {
                            print
"No refund is available because they are past the withdraw deadline\n";
                            &withdrawStudent;    # 	change SSTAT to withdrew

                        }
                        exit 1;
                    }

                }    #end while
            }       #end if for New Student+Paid
            else    #else if the new student has not paid yet
            {

                &feeNew;
                if ( $untilDayNew < 1 )    # new fall deadline at midnight
                {
                    $untilHourNew = ( ( $feeNew - $time ) / 3600 );
                    printf "There is %.2f hour(s) until the fee deadline.\n",
                      $untilHourNew;

                    if ( "International" eq $checkhash->{ISTAT} ) {
                        printf
                          "Student has not paid their fee of \$%.2f yet.\n",
                          $tuitionInt;
                    }
                    else {
                        printf
                          "Student has not paid their fee of \$%.2f yet.\n",
                          $tuitionReg;
                    }
                }
                elsif ( $untilDayNew >
                    1 )    # Still more than one day before new fall deadline
                {
                    printf
"There is %.0f day(s) until the fee deadline for NEW fall semester students, which is August 5 of this year. Please make sure student pays before this date or their seat will be removed\n",
                      $untilDayNew;
                    if ( "International" eq $checkhash->{ISTAT} ) {
                        printf
                          "Student has not paid their fee of \$%.2f yet.\n",
                          $tuitionInt;
                    }
                    else {
                        printf
                          "Student has not paid their fee of \$%.2f yet.\n",
                          $tuitionReg;
                    }

                }
                else    #past deadline
                {
                    print
"It is past the fee deadline, student seat has been removed.";
                }
            }    #end if for New Student+Unpaid
        }    #end if for New student
        elsif ( "Continuing" eq $checkhash->{SSTAT} ) {
            if ( "2" eq $checkhash->{SEM} ) {
                print "Student is starting semester 2.\n";

                if ( "Paid" eq $checkhash->{FSTAT} ) {
                    print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
                    chomp( $opt = <STDIN> );
                    while ( $opt !~ /Exit/i )  #Begin while loop for menu option
                    {
                        if ( $opt =~ /add/i ) {
                            &addropWin;
                            if ( $untilDayEnd >= 1 && $untilDayEnd <= 10 ) {
                                print "Student is eligible to add courses.\n";

                                printf
"There is %.0f day(s) until the add/drop deadline for semester 2 students, which is January 16 of this year. Please make sure student pays before this date or their seat will be removed\n",
                                  $untilDayEnd;
                                if ( "International" eq $checkhash->{ISTAT} ) {
                                    print
"Each course will cost \$1396.3 because student is finternational\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                }
                                else {
                                    print
"Each course will cost \$587.79 because student is non-international\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &addCourse;

                                    }
                                }
                            }
                            elsif ( $untilDayStart > 1 ) {
                                printf
"The add/drop period for semester 2 has not begun yet. There is currently %.0f days until the add/drop period.\n",
                                  $untilDayStart;
                                last;

                            }
                            else {
                                print
"Cannot perform action because add/drop period expired.\n";

                            }

                        }    #end if add
                        elsif ( $opt =~ /drop/i ) {
                            &addropWin;
                            if (   $untilDayEnd >= 1
                                && $untilDayEnd <=
                                10 )    #add/drop period for winter sem begins
                            {
                                print "Student is eligible to drop courses\n";

                                if ( "International" eq $checkhash->{ISTAT} ) {
                                    print
"Each course will cost \$1396.3 because student is an international student\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                }
                                else {
                                    print
"Each course will cost \$587.79 because student is a non-international student\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse2;
                                        &dropCourse;

                                    }
                                }
                            }
                            elsif ( $untilDayStart > 1 ) {
                                printf
"The add/drop period for semester 2 has not begun yet. There is currently %.0f days until the add/drop period.\n",
                                  $untilDayStart;
                                last;
                            }
                            else {
                                print
"Cannot perform action because add/drop period has expired.\n";
                                last;
                            }
                        }
                        elsif ( $opt =~ /Withdraw/i ) {
                            &withdrawWin;
                            if ( $untilWithdrawWinStart >
                                1 )    # if before program start, full refund
                            {
                                printf
"This person's program has not started yet, therefore he/she is eligible for a FULL refund. There is currently %.0f days until the student's program starts\n",
                                  $untilWithdrawWinStart;
                                &withdrawStudent;    #	change SSTAT to withdrew
                            }
                            elsif ($untilWithdrawWinEnd >= 1
                                && $untilWithdrawWinEnd <=
                                71 )   # elsif before withdraw date, half refund
                            {
                                printf
"This person's program has already started, therefore he/she is eligible for HALF refund. There is currently %.0f days until the Withdraw deadline and the refund policy will no longer be active\n",
                                  $untilWithdrawWinEnd;
                                &withdrawStudent;    #	change SSTAT to withdrew

                            }
                            else                     # else no refund
                            {
                                print
"No refund is available because they are past the withdrawal deadline\n";
                                &withdrawStudent;    # 	change SSTAT to withdrew

                            }
                            exit 1;
                        }
                        else {                       #error check
                            print
"The program cannot determine that function. Please re-enter.\n";
                        }
                    }    #end while
                    print "Program has terminated\n";
                }    #end if statement for paid

                elsif ( "Unpaid" eq $checkhash->{FSTAT} ) {
                    &feeWin;
                    if ( $untilDayNew < 1 )    # new fall deadline at midnight
                    {
                        $untilHourNew = ( ( $feeNew - $time ) / 3600 );
                        printf
                          "There is %.2f hour(s) until the fee deadline.\n",
                          $untilHourNew;

                        if ( "International" eq $checkhash->{ISTAT} ) {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionInt;
                        }
                        else {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionReg;
                        }
                    }
                    elsif ( $untilDayNew >
                        1 )   # Still more than one day before new fall deadline
                    {
                        printf
"There is %.0f day(s) until the fee deadline for NEW fall semester students, which is August 5 of this year. Please make sure student pays before this date or their seat will be removed\n",
                          $untilDayNew;
                        if ( "International" eq $checkhash->{ISTAT} ) {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionInt;
                        }
                        else {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionReg;
                        }

                    }
                    else    #past deadline
                    {
                        print
"It is past the fee deadline, student seat has been removed.";
                    }
                }    #end if unpaid
            }    #end if semester 2

            elsif ( "3" eq $checkhash->{SEM} ) {
                print "Student is starting semester 3.\n";
                if ( "Paid" eq $checkhash->{FSTAT} ) {
                    print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
                    chomp( $opt = <STDIN> );
                    while ( $opt !~ /exit/i )  #Begin while loop for menu option
                    {
                        if ( $opt =~ /add/i ) {
                            &addropFall;
                            if (   $untilDayEnd >= 1
                                && $untilDayEnd <=
                                10 )    # if add/drop fall period is active
                            {
                                print "Student is eligible to add courses.\n";
                                if ( "International" eq $checkhash->{ISTAT}
                                  )     # if international
                                {
                                    print
"Each course will cost \$1396.3 because student is international.\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &addCourse;

                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &addCourse;

                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &addCourse;

                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &addCourse;
                                    }
                                }       # end international case
                                else    # else, non-international
                                {
                                    print
"Each course will cost \$587.79 because student is non-international.\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &addCourse;

                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &addCourse;
                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;

                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &addCourse;
                                    }
                                }    #end non-international
                            }
                            elsif ( $untilDayStart > 1 ) {
                                printf
"The add/drop period for semester 3 has not begun yet. There is currently %.0f days until the add/drop period.\n",
                                  $untilDayStart;
                                last;
                            }
                            else     # add/drop fall period finished
                            {
                                print
"The add/drop period has expired. The function can no longer be completed.\n";
                                last;
                            }

                        }    #end if add
                        elsif ( $opt =~ /Drop/i ) {
                            &addropFall;
                            if ( $untilDayEnd >= 1 && $untilDayEnd <= 10 ) {
                                print "Student is eligible to drop courses.\n";
                                if ( "International" eq $checkhash->{ISTAT} ) {
                                    print
"Each course will cost \$1396.3 because student is international.\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;

                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;
                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;
                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;
                                    }
                                }
                                else {
                                    print
"Each course will cost \$587.79 because student is non-international\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;
                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;
                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;
                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse3;
                                        &dropCourse;
                                    }
                                }
                            }
                            elsif ( $untilDayStart > 1 ) {
                                printf
"The add/drop period for semester 3 has not begun yet. There is currently %.0f days until the add/drop period.\n",
                                  $untilDayStart;
                                last;
                            }
                            else {
                                print
"The add/drop period for semester 3 has expired. Function can no longer be completed.\n";
                                last;
                            }
                        }    #end elsif drop
                        elsif ( $opt =~ /Withdraw/i ) {
                            &withdrawFall;
                            if ( $untilWithdrawFallStart >
                                1 )    # if before program start, full refund
                            {
                                printf
"This person's program has not started yet, therefore he/she is eligible for a FULL refund. There is currently %.0f days until the student's program starts\n",
                                  $untilWithdrawFallStart;
                                &withdrawStudent;    #	change SSTAT to withdrew

                            }
                            elsif ($untilWithdrawFallEnd >= 1
                                && $untilWithdrawFallEnd <=
                                71 )   # elsif before withdraw date, half refund
                            {
                                printf
"This person's program has already started, therefore he/she is eligible for HALF refund. There is currently %.0f days until your Withdraw deadline refund policy ends\n",
                                  $untilWithdrawFallEnd;
                                &withdrawStudent;    #	change SSTAT to withdrew

                            }
                            else                     # else no refund
                            {
                                print
"No refund is available because they are past the withdraw deadline\n";
                                &withdrawStudent;    # 	change SSTAT to withdrew

                            }
                        }         #end elseif withdraw
                        else {    #error check
                            print "Please enter a valid option.\n";
                        }
                    }    #end while
                    print "Program has terminated\n";
                }    #end if statement for paid

                elsif ( "Unpaid" eq $checkhash->{FSTAT} ) {
                    &feeFall;
                    if ( $untilDayFall < 1 )    # new fall deadline at midnight
                    {
                        $untilHourFall = ( ( $feeFall - $time ) / 3600 );
                        printf
                          "There is %.2f hour(s) until the fee deadline.\n",
                          $untilHourFall;

                        if ( "International" eq $checkhash->{ISTAT} ) {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionInt;
                        }
                        else {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionReg;
                        }
                    }
                    elsif ( $untilDayFall >
                        1 )   # Still more than one day before new fall deadline
                    {
                        printf
"There is %.0f day(s) until the fee deadline for continuing fall semester students, which is September 2 of this year. Please make sure student pays before this date or their seat will be removed\n",
                          $untilDayFall;
                        if ( "International" eq $checkhash->{ISTAT} ) {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionInt;
                        }
                        else {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionReg;
                        }

                    }
                    else    #past deadline
                    {
                        print
"It is past the fee deadline, student seat has been removed.";
                    }
                }    #end unpaid
            }    #end if semester 3
            elsif ( "4" eq $checkhash->{SEM} ) {
                print "You are starting semester 4.\n";
                if ( "Paid" eq $checkhash->{FSTAT} ) {
                    print "Select a task for perform: Add/Drop/Withdraw/Exit\n";
                    chomp( $opt = <STDIN> );
                    while ( $opt !~ /exit/i )  #Begin while loop for menu option
                    {
                        if ( $opt eq "Add" ) {
                            &addropWin;
                            if ( $untilDayEnd >= 1 && $untilDayEnd <= 10 ) {
                                print "Student is eligible to add a course.\n";
                                if ( "International" eq $checkhash->{ISTAT} ) {
                                    print
"Each course will cost \$1396.3 because student is an international student\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;
                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;    #sub addCourse
                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;    # sub addCourse
                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;    #sub addCourse
                                    }
                                }
                                else                   #else non-international
                                {
                                    print
"Each course will cost \$587.79 because student is non-international.\n";
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;
                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;
                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;
                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &addCourse;
                                    }
                                }
                            }
                            else {
                                print
"Student can no longer add a course because the add/drop period has expired.\n";
                                last;
                            }

                        }    #end if add
                        elsif ( $opt =~ /drop/i ) {
                            &addropWin;
                            if ( $untilDayEnd >= 1 && $untilDayEnd <= 10 ) {
                                print "Student is eligible to drop a course\n";
                                if ( "International" eq $checkhash->{ISTAT} ) {
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    # sub dropCourse
                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    #sub dropcourse
                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    #sub dropCourse
                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    #sub dropCourse
                                    }
                                } #end if for international student+adding course
                                else {
                                    if ( "CS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    #sub dropCourse
                                    }
                                    elsif ( "SD" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    # sub dropCourse
                                    }
                                    elsif ( "NS" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    # sub dropCourse
                                    }
                                    elsif ( "Telecom" eq $checkhash->{MAJ} ) {
                                        &prgMajandCourse4;
                                        &dropCourse;    # sub dropCourse
                                    }
                                }
                            }
                            else {
                                print
"The period for adding and dropping courses has expired. Program will exit.\n";
                                last;
                            }
                        }
                        elsif ( $opt =~ /Withdraw/i ) {
                            &withdrawWin;
                            if ( $untilWithdrawWinStart >
                                1 )    # if before program start, full refund
                            {
                                printf
"This person's program has not started yet, therefore he/she is eligible for a FULL refund. There is currently %.0f days until student's program starts\n",
                                  $untilWithdrawWinStart;
                                &withdrawStudent;    #	change SSTAT to withdrew

                            }
                            elsif ($untilWithdrawWinEnd >= 1
                                && $untilWithdrawWinEnd <=
                                71 )   # elsif before withdraw date, half refund
                            {
                                printf
"This person's program has already started, therefore he/she is eligible for HALF refund. There is currently %.0f days until the refund policy ends\n",
                                  $untilWithdrawWinEnd;
                                &withdrawStudent;    #	change SSTAT to withdrew

                            }
                            else                     # else no refund
                            {
                                print
"No refund is available because they are past the withdraw deadline\n";
                                &withdrawStudent;    # 	change SSTAT to withdrew

                            }

                        }
                        else {                       #error check
                            print
"Please enter a valid option, and make sure you match the case\n";
                        }
                    }    #end while
                }    #end if statement for paid

                elsif ( "Unpaid" eq $checkhash->{FSTAT} ) {
                    &feeWin;

                    if ( $untilDayWin < 1 )    # winter deadline at midnight
                    {
                        $untilHourWin = ( ( $feeWin - $time ) / 3600 );
                        printf
                          "There is %.2f hour(s) until the fee deadline.\n",
                          $untilHourWin;

                        if ( "International" eq $checkhash->{ISTAT} ) {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionInt;
                        }
                        else {
                            printf
                              "Student has not paid their fee of \$%.2f yet.\n",
                              $tuitionReg;
                        }
                    }
                }    # end elsif unpaid
            }    #end if semester 4
            else {
                print "invalid input on the main file";
            }
        }    #end elsif continuing
        elsif ( "Expelled" eq $checkhash->{SSTAT} ) {
            print "Student is expelled. Delete his/her records accordingly\n";

        }
        elsif ( "Withdrew" eq $checkhash->{SSTAT} ) {
        	print "Student has withdrawn from their program.\n";
        }
        else {
        	print "Could not determine student's status.\n";
        }
    }    #end if statement for id check
}    #end foreach

if ( $flag == 1 ) {
    print
"That Student ID is currently unregistered. Please make sure that student is enrolled or that you have entered the correct 9 digit student ID \n";
}

